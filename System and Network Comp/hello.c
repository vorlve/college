#include <stdio.h>
#include <fcntl.h>
#include <pthread.h>
#include <string.h>

void* thread_func(void* arg);

int main()
{
    pthread_t thread;
    char *str = "Hello Thread";
    pthread_create(&thread, NULL, thread_func, str);
    pthread_join(thread,NULL);
    return 0;
}

void* thread_func(void* arg){
    char* str = (char*) arg;
    size_t s = strlen(str);
    int fd = creat("hello.txt", 0644);
    write(fd,str,s);
    close(fd);
    return 0;
}
