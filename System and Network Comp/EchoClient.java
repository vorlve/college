import java.net.Socket;
import java.io.*;


public class EchoClient {
    public static void main(String[] args) {
        try {
            System.out.println("Client Started");
            Socket soc = new Socket("localhost", 9806);
            BufferedReader inputUser = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Enter a string: ");
            String str = inputUser.readLine();
            PrintWriter out = new PrintWriter(soc.getOutputStream(), true);
            out.println(str);
            BufferedReader in = new BufferedReader(new InputStreamReader(soc.getInputStream()));
            System.out.println(in.readLine());
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }
}
