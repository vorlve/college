---
title: Assignment Prak SKJ
subtitle: TCP
author: Ronggo Tsani Musyafa -- `21/473988/PA/20449`
date: 10-11-2022
---
# Soal

2. Baca cara menghitung Estimated RTT dari materi suplemen yang dishare di elok dan tonton video youtubenya. Hitung Estimated RTT hingga segmen TCP terakhir yang mengandung data pada tracefile yang dishare di elok. Sama seperti Activity 6.2, segmen TCP pertama adalah segmen pertama yang memuat data (abaikan segmen-segmen yang terlibat dalam three-way handshake). Plot grafik yang menunjukkan hubungan Sample RTT dan Estimated RTT seperti di Figure 3.32 pada materi suplemen. Gunakan nilai alpha=0.125.

**Jawab:**

![](image/20221110145637.png) 

3. Baca cara menghitung timeout pada materi suplemen dan plot nilai timeoutnya pada grafik yang sama dengan grafik dari nomor 2. Analisis grafik yang anda dapatkan dan lihat apakah ada segmen yang terkena timeout berdasarkan perhitungan timeout tersebut? Jika ada, segmen/packet nomor berapa saja? Asumsikan nilai timeout untuk segmen pertama dan kedua adalah 1s (1.000ms). Setelah ACK untuk segmen kedua diterima, nilai timeout dihitung berdasarkan rumus pada halaman 237 materi suplemen dan diset sebagai timeout untuk segmen ketiga dst. *Gunakan nilai beta=0.25.

**Jawab:**

![](image/20221110145651.png)
Tidak ada timeout pada trace file ini karena tidak ada perpotongan antara grafik timeout dengan grafik RTT.

# Langkah Analisis

1. Membuka Trace File yang telah disediakan
2. Mencari paket yang membawa data dan rtt nya dengan menggunakan fungsi wireshark yaitu `tcp.analysis.ack_rtt`
3. Membuat code untuk menghitung estimated rtt dan timeout interval
   1. Timeout untuk segmen pertama dan kedua = 1 s
    \begin{align*}
        Timeout &= EstimatedRTT + 4 * DevRTT \\
        1 s &= 0.28624s + 4 * DevRTT \\
        \frac{0.71376}{4} &= DevRTT  \\
        DevRTT &= 0.17844
    \end{align*}


