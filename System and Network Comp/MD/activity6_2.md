---
title: Assignment Prak SKJ
subtitle: TCP
author: Ronggo Tsani Musyafa -- `21/473988/PA/20449`
date: 04-11-2022
geometry: margin=1.5cm
---

6. Consider the TCP segment containing the HTTP “POST” as the first segment in the data transfer part of the TCP connection.
   • At what time was the first segment (the one containing the HTTP POST) in the data-transfer part of the TCP connectionsent?
    
    **Jawab:**

    ![](image/20221104150258.png)  

    Arrival Time: Feb  3, 2021 09:43:26.716922000 WIB with Time since reference or first frame: 0.024047000 seconds

   • At what time was the ACK for this first data-containing segment received?
    
    **Jawab:** 

    Arrival Time: Feb  3, 2021 09:43:26.745546000 WIB with Time since reference or first frame: 0.052671000 seconds
    
   • What is the RTT for this first data-containing segment?

    **Jawab:**

    ![](image/20221104151316.png)  

    The RTT to ACK the segment was: 0.028624000 seconds
    
   • What is the RTT value the second data-carrying TCP segment and its ACK?
    
    **Jawab:**
    
    ![](image/20221104151455.png)  

    The RTT to ACK the segment was: 0.028628000 seconds

   • What is the EstimatedRTT value after the ACK for the second datacarrying segment is received? Assume that in making this calculation after the received of the ACK for the second segment, that the initial value of EstimatedRTT is equal to the measured RTT for the first segment, and then is computed using the EstimatedRTT equation (page 236 of the attached PDF file in elok), and a value of $\alpha$ = 0.125.
    
    **Jawab:**

    $$\text{EstimatedRTT} = (1 – \alpha) * \text{EstimatedRTT} + \alpha * \text{SampleRTT} $$
    $$\text{EstimatedRTT} = (1-0.125) * 28.624 + 0.125 * 28.628 $$
    $$\text{EstimatedRTT} = 28.6245 \text{ ms} $$

    
   Note: Wireshark has a nice feature that allows you to plot the RTT for each of the TCP segments sent. Select a TCP segment in the “listing of captured packets” window that is being sent from the client to the gaia.cs.umass.edu server. Then select: Statistics->TCP Stream Graph -> Round Trip Time Graph.


7. What is the length (header plus payload) of each of the first four data-carrying TCP segments?
    
    **Jawab:** 
    
    ![](image/20221104153602.png)  

    All of the TCP segment length is 1480

8. Are there any retransmitted segments in the trace file? What did you check for (in the trace) in order to answer this question?
    
    **Jawab:**

    ![](image/20221104154445.png)  
    ![](image/20221104154547.png)  

    There is not retransmitted segments in the trace file since in the time sequence graph (stevens), all sequence numbers are monotonically increasing. From analysis in Wireshark, there is not retransmitted segment.


