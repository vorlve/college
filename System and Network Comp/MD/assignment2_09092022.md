---
author: Ronggo Tsani Musyafa
title: Assignment 2 
subtitle: Praktikum SKJ
date: 09-09-2022
---

## Soal 1

Perhatikan RAW data (window terbawah Wireshark) untuk paket yang merupakan response pada problem no.3 di Activity 2.1. Dari mana didapatkan nilai 128 bytes untuk konten file html tersebut? Jelaskan!

- Response: 

![](image/20220911214707.png)  

**Jawab:** 128 bytes yang terdapat pada konten file didapat dari konten html yang terdapat di link tersebut mulai dari tag html hingga penutup tag tersebut.

## Soal 2

Lakukan Activity 2.2 dengan browser chrome. Masukkan url yang sama beberapa kali (minimal 2 kali) dengan menekan enter pada browser (bukan reload/F5). Chrome terkenal dengan sifatnya yang resource-intensive, coba analisis kenapa bisa seperti itu dilihat dari sisi bagaimana chrome mengirim GET. Lakukan juga pengujian pada browser lainnya seperti microsoft edge, perbedaan apa yang bisa anda lihat pada browser-browser tersebut?

- Request GET Chrome/Brave:

![](image/20220911220934.png) 
 
- Request GET Firefox:

![](image/20220911221436.png)  


**Jawab:** Request GET pada chrome dikirim 3 kali setiap memasukan url yang sama(bukan reload) sedangkan pada firefox hanya dikirim 1 kali. Hal tersebut terjadi karena pada firefox data tersebut masih disimpan (dalam cache) jika tidak diload lagi, sedangkan chrome meminta data tersebut lagi.

## Soal 3

Secara teori, nilai maksimal packet TCP bisa sampai 64kB. Selidiki kenapa packet TCP dikirim dalam satuan yang jauh lebih kecil (1500an B)?

**Jawab:** TCP sangat membantu ketika suatu file dikirim melewati network. Setiap file yang dikirim melalui protokol TCP akan dibagi menjadi beberapa bagian segmen pada layer transport dan akan digabungkan kembali apabila file tersebut sudah sampai. Ukuran maksimum segmen yang dapat dikirim melalui TCP adalah 64kB. Namun, pada layer di bawahnya terdapat MTU atau Maximum Transmission Unit umumnya hanya dapat mengirim 1500an B. File pada layer transport tadi akan dibagi lagi menjadi sebuah fragmen-fragmen yang ukurannya mengikuti MTU jika ukuran segmennya lebih besar daripada ukuran MTU. Oleh karena itu, ketika ada satu fragmen yang gagal dikirim maka satu segmen file itu akan dikirim ulang sehingga hal tersebut menjadi tidak efisien. Untuk menghindari ketidakefisienan tersebut, ukuran dari TCP itu diatur sehingga tidak akan melebihi MTU.

## Soal 4

Lakukan Activity 2.4 secara mandiri di rumah dan upload hasilnya di Activity 2.4. Pelajari metode encoding Base64 dan tunjukkan hasil encoding (secara manual) username dan password berikut:

- user: mahasiswa-ugm
- pass: dike

Tunjukkan step-step encodingnya secara rinci. Lakukan juga proses decoding pada data yang sudah di-encoding tersebut. Tunjukkan step-step decodingnya secara rinci.

**Jawab:** Credentials: mahasiswa-ugm:dike

1. Untuk menencoding username dan password tersebut, hal yang pertama kita lakukan adalah mengubah string tersebut menjadi barisan binary berukuran 8 digit.

    | String | Ascii | Binary   |
    | ------ | ----- | -------- |
    | m      | 109   | 01101101 |
    | a      | 97    | 01100001 |
    | h      | 104   | 01101000 |
    | a      | 97    | 01100001 |
    | s      | 115   | 01110011 |
    | i      | 105   | 01101001 |
    | s      | 115   | 01110011 |
    | w      | 119   | 01110111 |
    | a      | 97    | 01100001 |
    | -      | 45    | 00101101 |
    | u      | 117   | 01110101 |
    | g      | 103   | 01100111 |
    | m      | 109   | 01101101 |
    | :      | 58    | 00111010 |
    | d      | 100   | 01100100 |
    | i      | 105   | 01101001 |
    | k      | 107   | 01101011 |
    | e      | 101   | 01100101 |

2. Setelah diubah menjadi 8 digit bilangan biner, bilangan-bilangan tersebut kita susun berurutan
   
    ```   
    011011010110000101101000011000010111001101101001011100
    101110111011000010010110101110101011001110110110100111
    01001100100011010010110101101100101
    ```

3. Setelah diurutkan kita bagi deretan bilangan biner tersebut menjadi 6 digit.
   
   ```
   011011-010110-000101-101000-011000-010111-001101-101001-
   011100-110111-011101-100001-001011-010111-010101-100111-
   011011-010011-101001-100100-011010-010110-101101-100101
    ``` 

4. Deretan 6 digit bilangan biner tersebut kita ubah kembali menjadi bilangan desimal dan menjadi karakter Base 64.

    | Biner    | Desimal | Base64 |
    | -------- | ------- | ------ |
    | 00011011 | 27      | b      |
    | 00010110 | 22      | W      |
    | 00000101 | 5       | F      |
    | 00101000 | 40      | o      |
    | 00011000 | 24      | Y      |
    | 00010111 | 23      | X      |
    | 00001101 | 13      | N      |
    | 00101001 | 41      | p      |
    | 00011100 | 28      | c      |
    | 00110111 | 55      | 3      |
    | 00011101 | 29      | d      |
    | 00100001 | 33      | h      |
    | 00001011 | 11      | L      |
    | 00010111 | 23      | X      |
    | 00010101 | 21      | V      |
    | 00100111 | 39      | n      |
    | 00011011 | 27      | b      |
    | 00010011 | 19      | T      |
    | 00101001 | 41      | p      |
    | 00100100 | 36      | k      |
    | 00011010 | 26      | a      |
    | 00010110 | 22      | W      |
    | 00101101 | 45      | t      |
    | 00100101 | 37      | l      |

5. Maka, Base64 dari mahasiswa-ugm:dike adalah
`bWFoYXNpc3dhLXVnbTpkaWtl`
