---
title: Praktikum SKJ
subtitle: DNS -- Task
author: Ronggo Tsani Musyafa -- `21/473988/PA/20449`
date: 30-09-2022 
---

# Activity 3

DNS query message:

![](image/20220930223340.png) 

DNS query response message: 

![](image/20220930223428.png)  

11. What is the destination port for the DNS query message? What is the source port of the DNS response message?

        Destination Port of DNS Query Message = 53
        Source Port of DNS Response Message   = 53

12. To what IP address is the DNS query message sent? Is this the IP address of your default local DNS server?

    IP Address dari DNS query message : 180.250.13.42

    ![](image/20220930222915.png)  

    IP addressnya merupakan local DNS server saya.

13. Examine the DNS query message. What “Type” of DNS query is it? Does the query message contain any “answers”?

    ![](image/20220930223053.png)  

    Tipe dari DNS query message ini adalah tipe A. Query message ini tidak memiliki "answers".

14. Examine the DNS response message to the query message. How many “questions” does this DNS response message contain? How many “answers”?

    ![](image/20220930223231.png)  

    DNS response message mempunyai 1 "questions" dan 1 "answers"

```
Last, let’s use nslookup to issue a command that will return a
type NS DNS record,
Enter the following command:
nslookup –type=NS umass.edu
and then answer the following questions :
```

DNS query message NS :

![](image/20220930223821.png)  

DNS query response message NS :

![](image/20220930223905.png)  

15. To what IP address is the DNS query message sent? Is this the IP address of your default local DNS server?

    Destination IP of DNS query message : 180.250.13.42

    ![](image/20220930222915.png)  

    IP addressnya merupakan local DNS server saya.

16. Examine the DNS query message. How many questions does the query have? Does the query message contain any “answers”?

    DNS query message mempunyai 1 "questions" dan tidak berisi "answers"

17. Examine the DNS response message. How many answers does the response have? What information is contained in the answers? How many additional resource records are returned? What additional information is included in these additional resource records?

    ![](image/20220930224947.png)  

    DNS Response Messsage memiliki 3 "answers" yaitu memberikan 3 RRs dalam tipe NS dan memberikan 1 additional resource records yang berupa Root dengan type OPT


# Assignment

Cari bagaimana cara mengoperasikan nslookup pada cmd Windows (atau OS yang digunakan PC anda). Lalu lakukan itterative query secara manual untuk me-resolve dcse.fmipa.ugm.ac.id sehingga bisa diketahui IP addressnya tanpa mengontak local DNS server.

Pada itterative query ini, anda harus memulai dari menanyakan IP address dari domain "id" (tanpa tanda kutip) kepada root server. Berdasarkan jawaban dari root server, tanyakan IP address dari domain "ac.id" ke server id dan seterusnya ke bawah hingga anda menemukan IP address dari dcse.fmipa.ugm.ac.id.

a. Tunjukkan step-by-step command apa saja yang anda masukkan beserta outputnya (screenshot)

    \pagebreak

    1. Mencari root server
        
        Command = `nslookup -type=NS .`

        ![](image/20220930225805.png)

    \pagebreak

    2. Mencari server id dari salah satu server root

        Command = `nslookup -type=NS id. a.root-servers.net`

       ![](image/20220930225850.png)  

    3. Mencari server ac.id dari salah satu server id dan
       Mencari server ugm.ac.id dari salah satu server ac.d

        Command: 
        
                nslookup -type=NS ac.id. d.dns.id

        Command: 
                
                nslookup -type=NS ugm.ac.id. d.dns.id

       ![](image/20220930225911.png)  

    4. Mencari server fmipa.ugm.ac.id dari salah satu server ugm dan 
       Mencari server dcse.fmipa.ac.id

        Command:

                nslookup -type=NS fmipa.ugm.ac.id. d.dns.id

        Command: 

                nslookup -type=NS dcse.fmipa.ugm.ac.id. d.dns.id
            
       ![](image/20220930225931.png)   

b. Total ada berapa name server yang anda kunjungi? Tunjukkan pula IP address dari server-server tersebut.
   
   1. Server DNS lokal : 127.0.0.53#53
   2. a.root-servers.net : 198.41.0.4
   3. d.dns.id  : 45.126.57.57#53
   4. ns1.ugm.ac.id : 202.43.92.2#53