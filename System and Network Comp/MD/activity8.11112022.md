---
title: Activity Prak SKJ
subtitle: Socket Programming
author: Ronggo Tsani Musyafa -- `21/473988/PA/20449`
date: 11-11-2022 
geometry: margin=1.5cm 
---

# Soal

Copy the same program in the Youtube video and modify it such that the client will send "yourName" and the server will reply with "Hello yourName." Replace yourName with your nickname. Upload the codes and the execution screenshots

![](image/20221111144029.png)  

# Source Code

## `EchoServer.java`

```java
import java.net.ServerSocket;
import java.net.Socket;
import java.io.*;

public class EchoServer {
    public static void main(String[] args) {
        try {
            System.out.println("Waiting for clients ...");
            ServerSocket ss = new ServerSocket(9806);
            Socket soc = ss.accept();
            System.out.println("Connection Established");
            BufferedReader in = new BufferedReader(new InputStreamReader(soc.getInputStream()));
            String str = in.readLine();
            PrintWriter out = new PrintWriter(soc.getOutputStream(), true);
            out.println("Server says: Hello " + str);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
```

## `EchoClient.java`

```java
import java.net.Socket;
import java.io.*;


public class EchoClient {
    public static void main(String[] args) {
        try {
            System.out.println("Client Started");
            Socket soc = new Socket("localhost", 9806);
            BufferedReader inputUser = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Enter a string: ");
            String str = inputUser.readLine();
            PrintWriter out = new PrintWriter(soc.getOutputStream(), true);
            out.println(str);
            BufferedReader in = new BufferedReader(new InputStreamReader(soc.getInputStream()));
            System.out.println(in.readLine());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

```