---
title: Assignment Prak SKJ
subtitle: DeadLock
author: Ronggo Tsani Musyafa -- `21/473988/PA/20449`
date: 02-11-2022 
---

# Soal 1
Buatlah sebuah program yang mengirimkan sebuah string "Hello Thread" ke sebuah thread yang akan menyimpan string tersebut ke sebuah file bernama "hello.txt"

```c
#include <stdio.h>
#include <fcntl.h>
#include <pthread.h>
#include <string.h>

void* thread_func(void* arg);

int main()
{
    pthread_t thread;
    char *str = "Hello Thread";
    pthread_create(&thread, NULL, thread_func, str);
    pthread_join(thread,NULL);
    return 0;
}

void* thread_func(void* arg){
    char* str = (char*) arg;
    size_t s = strlen(str);
    int fd = creat("hello.txt", 0644);
    write(fd,str,s);
    close(fd);
    return 0;
}
```
![](image/20221102154241.png)  

# Soal 2
Buatlah sebuah program yang mengilustrasikan sebuah deadlock dengan menggunakan tiga threads. 
```c
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>

void *function1();
void *function2();
void *function3();

pthread_mutex_t res_a;
pthread_mutex_t res_b;
pthread_mutex_t res_c;

int main()
{

    pthread_mutex_init(&res_a, NULL);
    pthread_mutex_init(&res_b, NULL);
    pthread_mutex_init(&res_c, NULL);

    pthread_t one, two, three;

    pthread_create(&one, NULL, function1, NULL); // create thread
    pthread_create(&two, NULL, function2, NULL);
    pthread_create(&three, NULL, function3, NULL);
    pthread_join(one, NULL);
    pthread_join(two, NULL);
    pthread_join(three, NULL);
    printf("Thread joined\n");
}

void *function1()
{
    pthread_mutex_lock(&res_a);
    printf("Thread ONE acquired res_a\n");
    sleep(1);

    pthread_mutex_lock(&res_b);
    printf("Thread ONE acquired res_b\n");

    pthread_mutex_lock(&res_c);
    printf("Thread ONE acquired res_c\n");

    pthread_mutex_unlock(&res_c);
    printf("Thread ONE acquired res_c\n");

    pthread_mutex_unlock(&res_b);
    printf("Thread ONE released res_b\n");

    pthread_mutex_unlock(&res_a);
    printf("Thread ONE released res_a\n");
}
void *function2()
{
    pthread_mutex_lock(&res_b);
    printf("Thread TWO acquired res_b\n");
    sleep(1);

    pthread_mutex_lock(&res_c);
    printf("Thread TWO acquired res_c\n");

    pthread_mutex_lock(&res_a);
    printf("Thread TWO acquired res_a\n");
    
    pthread_mutex_unlock(&res_a);
    printf("Thread TWO released res_a\n");

    pthread_mutex_unlock(&res_c);
    printf("Thread TWO released res_c\n");

    pthread_mutex_unlock(&res_b);
    printf("Thread TWO released res_b\n");
}

void *function3()
{
    pthread_mutex_lock(&res_c);
    printf("Thread THREE acquired res_c\n");
    sleep(1);

    pthread_mutex_lock(&res_a);
    printf("Thread THREE acquired res_a\n");

    pthread_mutex_lock(&res_b);
    printf("Thread THREE acquired res_b\n");
    
    pthread_mutex_unlock(&res_b);
    printf("Thread THREE released res_b\n");
 
    pthread_mutex_unlock(&res_a);
    printf("Thread THREE released res_a\n");

    pthread_mutex_unlock(&res_c);
    printf("Thread THREE released res_c\n");
}
```
![](image/20221102155502.png)  