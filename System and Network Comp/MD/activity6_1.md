---
title: Assignment Prak SKJ
subtitle: TCP
author: Ronggo Tsani Musyafa -- `21/473988/PA/20449`
date: 04-11-2022
---

# Soal

- HTTP
  
![](image/20221104141822.png)  

1. What is the IP address and TCP port number used by the client computer (source)
   that is transferring the alice.txt file to gaia.cs.umass.edu? To answer this question,
   it’s probably easiest to select an HTTP message and explore the details of the
   TCP packet used to carry this HTTP message, using the “details of the selected
   packet header window.”

    **Jawab:**

    Source Address: `10.6.139.94`
    Source Port: `38006`

2. What is the IP address of gaia.cs.umass.edu? On what port number is it sending
   and receiving TCP segments for this connection?

    **Jawab:**

    IP address of gaia.cs.umass.edu: `128.119.245.12`
    Port Number of gaia.cs.umass.edu: `80`

3. What is the sequence number of the TCP SYN segment that is used to initiate the
   TCP connection between the client computer and gaia.cs.umass.edu? (Note: this
   is the “raw” sequence number carried in the TCP segment itself; it is NOT the
   packet # in the “No.” column in the Wireshark window. Remember there is no
   such thing as a “packet number” in TCP or UDP; as you know, there are sequence
   numbers in TCP and that’s what we’re after here. Also note that this is not the
   relative sequence number with respect to the starting sequence number of this
   TCP session.). What is it in this TCP segment that identifies the segment as a
   SYN segment?

    **Jawab:**

    ![](image/20221104142606.png)  

    Sequence Number (raw): 380121428 

    From the Flags we know that this TCP segment is identifies as SYN segment because syn set.

4. What is the sequence number of the SYNACK segment sent by gaia.cs.umass.edu
   to the client computer in reply to the SYN? What is it in the segment that
   identifies the segment as a SYNACK segment? What is the value of the
   Acknowledgement field in the SYNACK segment? How did gaia.cs.umass.edu
   determine that value?
    
    **Jawab:** 

    ![](image/20221104142334.png)  

    Sequence number of the SYNACK segment : 824602597 (raw) 

    It is SYNACK segment because in the flags, syn and ack set.

    The value of Acknowledgement field : 380121429.

    Server determine the value of Acknowledgemnt field from the last ACK'd segment is 380121428 increment by one that expect the next segment.


5. What is the sequence number of the TCP segment containing the header of the
   HTTP POST command? Note that in order to find the POST message header,
   you’ll need to dig into the packet content field at the bottom of the Wireshark
   window, looking for a segment with the ASCII text “POST” within its DATA
   field 1,2. How many bytes of data are contained in the payload (data) field of this
   TCP segment? Did all of the data in the transferred file alice.txt fit into this single
   segment?

    **Jawab:**

    ![](image/20221104143844.png)  

    Sequence Number (raw): 380272103

    TCP payload : 2360 bytes

    The data didn't fit into this single segment because the size of the data is 152Kb but TCP payload is 2360 bytes.