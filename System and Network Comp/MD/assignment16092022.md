---
title: Praktikum SKJ
subtitle: Introduce to Linux
author: Ronggo Tsani Musyafa
date: 18-09-2022 
---

1. Di folder home, buatlah sebuah file teks bernama myname.txt dan tulis namamu di dalamnya. Kemudian, jalankan dan simpan hasil strace untuk perintah copy (cp) untuk menyalin file myname.txt tersebut dari folder home ke folder Documents. Simpan hasil strace tersebut dalam sebuah file bernama copy.log. Kemudian filter file copy.log untuk mencari path Documents di dalamnya. Jelaskan apa yang dilakukan oleh system calls hasil filter tersebut! Sertakan tangkapan layar proses yang dilakukan.

    ![](image/20220918215324.png)  

    1. `execve()`

        System call ini berfungsi untuk mengeksekusi program yang mengacu pada *pathname*. Pada proses copy file `myname.txt` ke folder Document. Sebanyak 46 environment variabel diteruskan ke program yang dieksekusi.
    
    2. `newsfstatat()`

        System call ini berfungsi untuk me*return* informasi mengenai file pada buffer yang dirujuk dengan *statbuf* atau address penyimpanannya. Pada pemanggilan fungsi pertama, fungsi tersebut memeriksa bahwa direktori yang dituju ada dan pada pemanggilan fungsi kedua, ditunjukkan bahwa file `myname.txt` belum ada.
    
    3. `openat()`

        System call ini berfungsi untuk membuka file yang diacu oleh *pathname*. Jika file tersebut tidak ada maka secara opsional akan dibuat filenya dengan system call `openat()`.

2. Buatlah sebuah program menggunakan system calls dengan bahasa C untuk menerima masukan dari keyboard dan menyimpan masukan tersebut ke dalam sebuah file bernama output.txt. Tuliskan source code program tersebut dalam file PDF dan Sertakan tangkapan layar saat program tersebut dikompilasi dan dijalankan, serta isi dari file output.txt.

    ```c
    #include <unistd.h>
    #include <sys/types.h>
    #include <sys/stat.h>
    #include <fcntl.h>


    int main() {

        int fd;
        char buf[100];
        int bytes_read = read(0, buf, 100);
        fd = open("output.txt", O_CREAT|O_WRONLY|O_TRUNC ,0644);
        write(fd, buf, bytes_read);
        close(fd);
    }
    ```

    ![](image/20220918224447.png)  
