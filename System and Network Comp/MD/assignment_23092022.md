---
title: Pratikum SKJ
subtitle: Prosess and Its Managemen -- Assignment
author: Ronggo Tsani Musyafa -- `21/473988/PA/20449`
date: 23-09-2022
---


# Tugas 4

1. Buatlah sebuah program X dengan perulangan dengan jumlah perulangan bebas namun cukup besar. Jalankan program tersebut dan cetak tabel proses menggunakan ps. Apakah status dari proses X setelah dijalankan? Kirimkan sinyal berhenti dan lanjutkan ke proses X. Bagaimanakah status dari proses X berubah? Jelaskan dengan memberikan beberapa tangkapan layar dari proses ini.

    ![](image/20220926212004.png)  

    \pagebreak

    Pada gambar di atas terlihat bahwa program `for` berawal dari status R yang berarti *Running* yaitu program `for` sedang berjalan dan ketika kita kirimkan sinyal *STOP* ke program `for` maka status pada proses berubah menjadi T yang berarti *Terminate* atau terhenti setelah itu kita kirimkan sinyal *CONT* sehingga program tersebut kembali berjalan sampai selesai.

2. Buatlah sebuah program yang membuat parent process A dan child process B. Di program ini, buatlah sehingga proses B membutuhkan waktu yang lebih lama untuk selesai daripada proses A dan proses A tidak memiliki wait system call. Tampilkan id proses A dan B di masing-masing blok kode mereka. Jalankan program tersebut dan ambil beberapa tangkapan layar. Apakah id proses dari parent process di blok kode proses B? Mengapa bisa seperti itu?
   
    ```c
    #include <stdio.h>
    #include <unistd.h>
    #include <sys/types.h>
    #include <sys/wait.h>

    int main() {

        pid_t p;
        printf("Starting the fork\n");
        p = fork();

        // block code for the child process
        if (p == 0) {
            sleep(10);
            printf("I am a child process: %d\n", getpid());
            printf("My parent id is %d\n", getppid());
        }
        // block of code for the parent process
        else {
            printf("I am parent process: %d\n", getpid());
            printf("My child id is %d\n", p);
        }
    }
    ```

    ![](image/20220926213235.png) 

    Pada gambar di atas parent id dari prosess child berbeda dengan parent sebelumnya. Hal ini bisa terjadi ketika proses parentnya lebih dulu terhenti sehingga proses child akan dilakukan *re-parenting*. Pada kasus ini parent baru dari prosess child adalah `systemd` hal itu dapat kita liat melalui `ps -aux`. 
