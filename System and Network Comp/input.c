#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>


int main() {

    int fd;
    char buf[100];
    int bytes_read = read(0, buf, 100);
    fd = open("output.txt", O_CREAT|O_WRONLY|O_TRUNC ,0644);
    write(fd, buf, bytes_read);
    close(fd);
}