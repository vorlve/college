import pandas as pd
import matplotlib.pyplot as plt

# Membaca data yang telah didapatkan
df = pd.read_csv(
    '/home/vorlve/Documents/Kuliah/Semester 3 /System and Network Comp/RTT.csv', 
    header=0, skiprows=[1])

# nilai alpha
alpha = 0.125

# array kosong untuk kolom estimated rtt
estimated_rtt = []
for i, row in df.iterrows():
    # jika i = 0, estimated rtt sama dengan RTT
    if i == 0:
        estimated_rtt.append(row['RTT'])
    else:
    # gunakan rumus estimated rtt
        estimated_rtt.append(
            (1 - alpha) * estimated_rtt[i - 1] + alpha * row['RTT'])

# membuat kolom estimated rtt di dataframe yang ada
df = df.assign(EstimatedRTT=estimated_rtt)

# plot grafik
plt.plot(df['RTT'], label='RTT')
plt.plot(df['EstimatedRTT'], label='EstimatedRTT')
plt.legend()
plt.show()

# nilai beta
beta = 0.25

# array kosong untuk dev rtt dan timeout interval
dev_rtt = []
timeout_interval = []

for i, row in df.iterrows():
    # jika i < 2 maka dev rttnya 0.17844 dan timeout intervalnya 1 s
    if i < 2:
        dev_rtt.append(0.17844)
        timeout_interval.append(1)
    else:
    # Mencari nilai dari dev rtt
        dev_rtt.append(
            (1-beta) * dev_rtt[i-1] + beta * abs(row['RTT'] - row['EstimatedRTT']))
    # Mencari nilai timeout interval
        timeout_interval.append(row['EstimatedRTT'] + 4 * dev_rtt[i])

# Membuat kolom timeout interval
df = df.assign(TimeoutInterval=timeout_interval)

# plot grafik
plt.plot(df['RTT'], label='RTT')
plt.plot(df['EstimatedRTT'], label='Estimated RTT')
plt.plot(df['TimeoutInterval'], label='Timeout Interval')
plt.legend()
plt.show()