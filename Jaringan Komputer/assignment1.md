---
title: Computer Network
subtitle: Task 1
author: Ronggo Tsani Musyafa -- `21/473988/PA/20449`
date: 30-09-2022 
---
# Soal

Tonton video youtube berikut dan hitung nilai UDP checksum untuk kasus yang ditunjukkan pada menit:detik ke 6:45 pada video tersebut. Namun, ganti konten data menjadi "Halo" (tanpa tanda petik). Asumsikan data diencode menggunakan ASCII.

<https://www.youtube.com/watch?v=rYVHBICiiEc>

# Jawaban

1. Source IP    = 192.169.0.31 (11000000.10101000.00000000.00011111)

        1100000010101000
        0000000000011111
        ---------------- +
        1100000011000111

2. Dest IP      = 192.169.0.30 (11000000.10101000.00000000.00011110)

        11000000 11000111   Penjumlahan dari source IP
        11000000 10101000
        ----------------- +
        10000001 01101111
                        1   Carry penjumlahan sebelumnya
        ----------------- +
        10000001 01110000   
        00000000 00011110
        ----------------- +
        10000001 10001110

\pagebreak

3. All Zeros and 8-bit protocol = 0000000000010001

        10000001 10001110   Penjumlahan dari Dest IP
        00000000 00010001
        ----------------- +
        10000001 10011111

4. UDP Length   = 0000000000001100 (12)

        10000001 10011111   Penjumlahan dari sebelumnya
        00000000 00001100
        ----------------- +
        10000001 10101011

5. Source Port  = 20 (00000000000010100)

        10000001 10101011   Penjumlahan dari UDP Length
        00000000 00010100
        ----------------- +
        10000001 10111111

6. Dest Port    = 10 (00000000000001010)

        10000001 10111111   Penjumlahan dari Source Port
        00000000 00001010
        ----------------- +
        10000001 11001001

7. UDP Length   = 0000000000001100

        10000001 11001001   Penjumlahan dari Dest Port
        00000000 00001100
        ----------------- +
        10000001 11010101

8. Data  "Halo" = 01001000 01100001 01101100 01101111 
    
        10000001 11010101   Penjumlahan dari UDP Length
        01001000 01100001
        ----------------- +
        11001010 00110110
        01101100 01101111
        ----------------- +
        00110110 10100101
                        1   Carry dari penjumlahan sebelumnya
        ----------------- +
        00110110 10100110

\pagebreak

9. Checksum

        00110110 10100110
                        1   One's Complement
        ----------------- +
        11001001 01011001


**Jadi**, Checksum dari data diatas adalah 1100 1001 0101 1001