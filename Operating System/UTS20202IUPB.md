---
title: Operating System
subtitle: Jawaban UTS 2020 IUP B
author: Ronggo Tsani Musyafa -- `21/473988/PA/20449`
date: 29-09-2022
---

# Soal

![](image/20220929202515.png)

# Jawaban

1. Perbedaan Short, Mid, Long term in Scheduling
   
    |Long Term|Short Term|Medium Term|
    |--- |--- |--- |
    |It is a job scheduler.|It is a CPU scheduler.|It is swapping.|
    |Speed is less than short term scheduler.|Speed is very fast.|Speed is in between both|
    |It controls the degree of multiprogramming.|Less control over the degree of multiprogramming.|Reduce the degree of multiprogramming.|
    |Absent or minimal in a time-sharing system.|Minimal in a time-sharing system.|Time-sharing system uses a medium-term scheduler.|
    |It selects processes from the pool and load them into memory for execution.|It selects from among the processes that are ready to execute.|Process can be reintroduced into the meat and its execution can be continued.|
    |Process state is (New to Ready).|Process state is (Ready to Running)|–|
    |Select a good prccess, mix of I/O bound, and CPU bound.|Select a new process for a CPU quite frequently.|–|

2. Mekanisme Kehadiran Content Switch pada proses P0 ke P1, P2 dan kembali ke P0