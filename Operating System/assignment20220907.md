---
author: 'Faiz Unisa Jazadi -- `21/475298/PA/20563`'
date: '2022-09-08'
subtitle: Tugas Sistem Pengoperasian
title: Threads
---

# Definisi

Thread adalah suatu jalur/path eksekusi dalam suatu proses. Suatu proses dapat
mempunyai beberapa thread. Sebuah thread juga sering dikenal sebagai sebuah
proses yang ringan. Sebagai contoh, dalam suatu browser, membuka banyak tab
dapat dilakukan menggunakan beberapa thread yang berbeda-beda. Aplikasi VS Code
menggunakan beberapa thread: untuk memproses input, untuk melakukan *linting*,
untuk *formatting*, dan sebagainya.

# Thread vs Process

Perbedaan utama terletak pada karakteristik pembagian ruang memori. Dua proses
yang berbeda berada pada dua ruang memori yang berbeda, sedangkan dua thread
dalam suatu proses berada pada ruang memori yang sama. Dengan kata lain, thread
tidak bersifat independen layaknya proses. Oleh karena itu, satu thread dan
thread lainnya saling berbagi code section, data section, dan resource OS.
Namun, mirip seperti proses, suatu thread memiliki program counter (PC),
register set, dan stack space-nya sendiri.

# Kelebihan (dibandingkan proses)

Berikut adalah beberapa kelebihan yang dimiliki oleh thread jika dibandingkan
dengan proses.

1.  Responsivitas: jika suatu proses dibagi menjadi beberapa thread, maka ketika
    suatu thread berakhir, output bisa langsung dikembalikan.
2.  Context switch yang lebih cepat karena waktu yang dibutuhkan lebih sedikit
    dibandingkan proses context switching untuk proses
3.  Utilisasi yang lebih efektif untuk sistem multiprosesor: suatu proses yang
    mempunyai beberapa thread dapat dijadwalkan untuk berjalan pada beberapa
    prosesor sehingga proses eksekusi menjadi lebih cepat.
4.  Pembagian resource: resource seperti data, kode, dan file dapat dibagi ke
    thread-thread dalam suatu proses
5.  Komunikasi: melakukan komunikasi antar thread lebih mudah karena sifat
    thread yang berbagi ruang memory dengan thread lainnya (tidak seperti
    komunikasi antar proses yang membutuhkan teknik khusus)
6.  Peningkatan throughput: jika suatu proses dibagi-bagi menjadi beberapa
    thread, dan setiap thread dianggap sebagai satu pekerjaan, maka banyak
    pekerjaan dalam per satuan waktu akan meningkat, yang pada berarti
    peningkatan throughput sistem.

# Tipe-tipe thread

Terdapat dua tipe thread, di antaranya adalah:

-   User level thread
-   Kerenel level thread

# Membutuhkan Thread

-   Ketika membutuhkan waktu yang jauh lebih cepat untuk membuat thread baru pada proses yang sudah ada dibandingkan dengan membuat proses yang baru
-   Threads dapat membagikan data yang umum, mereka tidak perlu menggunakan komunikasi antar proses
-   Pergantiaan konten lebih cepat ketika menggunakan Threads
-   Waktu yang dibutuhkan untuk mengakhiri threads lebih cepat daripada mengakhiri proses.

# Component Thread

-   Program Counter
-   Register set
-   Stack space

# Referensi

https://www.geeksforgeeks.org/thread-in-operating-system/
https://www.javatpoint.com/threads-in-operating-system/ 
