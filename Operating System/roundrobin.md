---
title: Scheduling
subtitle: Round Robin
author: Ronggo Tsani Musyafa -- `21/473988/PA/20449`
date: 29-09-2022
---

# Round Robin Scheduling

- CPU is assigned to the process on the basis of FCFS for a fixed amount of time.
- This fixed amount of time is called as time quantum or time slice.
- After the time quantum expires, the running process is preempted and sent to the ready queue.
- Then, the processor is assigned to the next arrived process.
- It is always preemptive in nature.

![](image/20220929192610.png)

| PID | AT  | BT               |
| --- | --- | ---------------- |
| P1  | 5   | 5 -> 2 -> 0      |
| P2  | 4   | 6 -> 3 -> 0      |
| P3  | 3   | 7 -> 4 -> 1 -> 0 |
| P4  | 1   | 9 -> 6 -> 3 -> 0 |
| P5  | 2   | 2 -> 0           |
| P6  | 6   | 3 -> 0           |

Ready Queue

P4,P5,P3,P2,P4,P1,P6,P3,P2,P4,P1,P3

| 0 1 | 1 4 | 4 6 | 6 9 | 9 12 | 12 15 | 15 18 | 18 21 | 21 24 | 24 27 | 27 30 | 30 32 | 32 33 |
| --- | --- | --- | --- | ---- | ----- | ----- | ----- | ----- | ----- | ----- | ----- | ----- |
|     | P4  | P5  | P3  | P2   | P4    | P1    | P6    | P3    | P2    | P4    | P1    | P3    |

# Deadlock Avoidance

| P # | Allocation | Maximum | Available | Needs |     |
| --- | ---------- | ------- | --------- | ----- | --- |
| --- | A B C      | A B C   | A B C     | A B C |     |
| P0  | 0 1 0      | 7 5 3   | 3 3 2     | 7 4 3 | x   |
| P1  | 2 0 0      | 3 2 2   | 5 3 2     | 1 2 2 | o   |
| P2  | 3 0 2      | 9 0 2   | 7 4 3     | 6 0 0 | x   |
| P3  | 2 1 1      | 2 2 2   | 7 4 5     | 0 1 1 | o   |
| P4  | 0 0 2      | 4 3 3   | 7 5 5     | 4 3 1 | x   |

        7 2 5                  10 5 7


Total = 10 5 7

safe sequence = P1,P3,P4,P0,P2
