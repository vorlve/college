---
title: Penerapan Algoritma Djikstra dan A*
author: Ronggo Tsani Musyafa
date: 12-09-2022 
marp: true
---

<!-- _class: head -->
# Penerapan Algoritma Djikstra dan A*

Ronggo Tsani Musyafa
21/473988/PA/20449


---
# Dijkstra Algorithm
Merupakan sebuah algoritma yang digunakan untuk mencari shortest path dari node awal atau source node sampai node yang ditargetkan.

Algoritma Dijkstra ini akan membuat sebuah tree dari shortest path yang telah terbentuk dengan meminimalisir jarak atau berat total dari source node ke semua node yang ada di graph.

---
# Dijkstra Implementation

Algoritma ini banyak diterapkan dalam dunia nyata seperti, mencari rute tercepat dari satu tempat ke tempat lainnya dan dalam membentuk social networking.
![w:600px bg left](image/20220912221848.png)  

---

# A* Algorithm

Merupakan sebuah algoritma yang berfungsi untuk mencari jalur atau rute. Algoritma ini merupakan perluasan daripada algoritma dijkstra dengan mempertimbangkan nilai heuristic.

---
# A* Implementation or Application

Aplikasi algoritma ini dapat ditemukan dari game-game yang ada seperti maze dan juga maps seperti *google maps* pun menggunakan algoritma A* ini. Selain itu, algoritma ini juga digunakan dalam network routing protocol untuk menghitung rute terbaik antara dua nodes.
![bg right w:38em](image/20220912222925.png)  
  
