---
title: Assignment AI
subtitle: Expert System 
author: Ronggo Tsani Musyafa -- `21/473988/PA/20449`
date: 31-10-2022 
bibliography: "AI.bib"
geometry: "left=3cm,right=3cm,top=2cm,bottom=2cm"
---

Paper yang dipilih:

S. Shishehchi and S. Y. Banihashem, `A rule based expert system based on ontology for diagnosis of ITP disease,` Smart Health, vol. 21, p. 100192, 2021. 

# Abstrak

Paper ini mempunyai sasaran untuk mengimplementasikan sistem pakar yang berbasis aturan semantik untuk mendiagnosa jenis penyakit yang seperti trombositopenia imun darah. Paper ini mempresentasikan ontologi untuk menggambarkan domain pengetahuan dari penyakit, gejala, dan bagaimana cara perawatannya. Sistem yang dikembangkan menggunakan data dari laboratorium rumah sakit Iran. Pada sistemnya, gejala diambil dari pasien melalui antarmuka pengguna yang berbasis Java. Sistem ini mendukung pasien untuk bertahan dari penyakit ini untuk mendapatkan jenis penyakit mereka dan merekomendasikan perawatan yang sesuai. Beberapa aturan semantik didefinisikan dan kemudian, dengan reasoner dapat menyimpulkan aturan untuk melakukan proses diagnostik. Proses diagnosis divalidasi oleh spesialis darah. Karena sistem ini dapat digunakan oleh pasien, dokter, atau mahasiswa kedokteran dimana saja, ini dapat membantu untuk membuat penyakit ini lebih mudah untuk diikuti dan berusaha untuk menghemat biaya dan waktu untuk pasien. Kuisioner juga dikembangkan untuk mengukur kegunaan dari sistem. Hasil dari kuisioner memuaskan setelah diuji dengan 154 responden. Uji reliabilitas dilakukan dan Alpha Cronbach adalah 0.865 yang lebih tinggi dari 0.7. Nilai rata-rata dari kuisioner lebih rai 4 dan rata-rata total adalah 4.49 dimana nilai tersebut dapat diterima untuk menjelaskan tingkat penerimaan pengguna dan akurasi sistem tinggi.

# Pembahasan

Penyakit darah apapun dapat berbahaya bagi manusia karena darah menyebar dan mengalir ke seluruh tubuh. Penyakit darah yang berupa Imun Thrombocytopenia (ITP) adalah semacam kelainan darah. Penyakit ini merupakan penyakit darah yang ringan yang mengarah ke pendarahan dan memar yang berlebihan. Pada ITP, meskipun ketika jumlah platelet turun, pasien tidak mempunyai gejala pendarahan apapun dan perawatan pun tidak dibutuhkan sampai ketika pendarahan dan memar muncul. Penyakit ini sangatlah menyakitkan karena Anda harus melakukan uji darah secara teratur. Berdasarkan progress dari penyakitnya, tipe dari perawatan seperti obat oral dan injeksi pun dapat berbeda. 

Pada paper ini tujuan dari sistem pakar yang didesain dan dikembangkan adalah untuk membantu dokter dalam mendiagnosa ITP dan juga pasien dapat menggunakan sistem ini secara langsung.

## Manfaat

- Memudahkan dokter dalam mendiagnosis penyakit ITP ini.
- Pasien dapat menggunakan sistem pakar ini dalam mendiagnosis penyakit ITP.

\pagebreak

## Metodologi

Paper ini mempunyai 5 tahapan dalam menyelesaikan prosess diagnosis ITP. Dibutuhkan pengetahuan mengenai ITP untuk membangun sistem pakar untukk ITP itu sendiri yang diambil dari pakar ahli seperti hematologis, intenist, siswa hematologi dan siswa intenist. Informasi yang mengenai penyakit ITP, gejala dan perawatannya disiman pada database dan setelahnya semua informasi akan disajikan ulang ke pengetahuan dengan ontologi. Proses selanjutnya adalah penalaran untuk mengestrak fakta baru. Dan langkah akhir merupakan pembuatan antarmuka pengguna grafis yang dikembangkan dengan Java. Dokter dan pasien dapat memasukkan gejala yang telah diamati ke sistem pakar melalui antarmuka pengguna dan mendapatkan perawatan yang sesuai

## Rule

![](image/20221031194845.png){width=50%}
![](image/20221031194858.png){width=50%}  
Ada 15 rule yang dirancang untuk mengembangkan sistem pakar agar dapat mendiagnosis penyakit ITP dan perawatan yang tepat.

# Kategori

Sistem pakar pada paper ini berkategori diagnosis dan debugging dan repair karena sistem pakar pada paper ini menentukan diagnosis penyakit ITP dan bagaimana perawatan yang tepat.