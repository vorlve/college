---
title: Assignment AI
subtitle: Natural Language Processing
author: Ronggo Tsani Musyafa -- `21/473988/PA/20449`
date: 13-11-2022
geometry: margin=2cm
---

# NLP

Cabang ilmu komputer yang berfokus pada pengembangan sistem yang memungkinkan komputer dapat berkomunikasi
dengan manusia menggunakan bahasa sehari-hari.

Disebut juga sebagai Computational Linguistitics dimana metode komputasi dapat membantu memahami bahasa manusia.

Memiliki tujuan yaitu komunikasi dalam pembuatannya.

### Related Areas

- Artificial Intelligence
- Formal Language Theory (Automata)
- Machine Learning
- Linguistics
- Psycholinguistics
- Cognitive Science
- Philosophy of language

### Kategori

- Natural Language Understanding
- Natural Language Generation

## Komunikasi

- Speaker :
  - Intention = Kapan dan informasi apa yang harus dikirim
  - Generation = Menerjemahkan informasi yang akan dikomunikasian
  - Synthesis = Output
- Listener :
  - Perception = Memetakan input menjadi serangkaian kata
  - Analysis = Menentukan konten informasi dari string menjadi :
    - Syntatic Interpretation = parse tree benar dan dapat menunjukkan struktur frasa string
    - Semantic Interpretation = mengestrak makna dari suatu string
    - Pragmatic Interpretation = mempertimbangkan efek dari keseluruhan konteks untuk mengubah makna dari suatu kalimat
  - Incorporation = Memutuskan apakah akan percaya dan menambahkan ke knowledge base

### Pemahaman Modular

![](image/20221113220513.png)

### Linguistic Level

![](image/20221114002308.png)

### Syntax

Menjelaskan bentuk dari Bahasa dari urutan yang tepat dari kata-kata dan pengaruhnya terhadap makna

Example:

- The dog bit the boy
- The boy bit the dog
- - Bit boy dog the the

### Semantics

Makna harfiah dari kata, frasa dan kalimat dalam suatu Bahasa. Membangun sistem pemrosesan bahasa alami menggunakan representasi yang sederhana

Example:

- `plant` sebagai suatu organisme yang berfotosintesis
- `plant` sebagai kegiatan menabur benih

### Pragmatics

Konteks komunikatif dan sosial secara keseluruhan serta pengaruh nya terhadap interpretasi. Pada analisis ini agen harus mempertimbangkan lebih dari hanya sekedar kalimat. Agen harus melihat lebih ke dalam ke dalam konteks kalimat.

`NLP melibatkan berbagai tugas sintaksis, semantik, pragmatis dan juga masalah-masalah lainnya`

## Syntatic Tasks

### Word Segmentation

Tindakan mengambil dan memecah string kalimat dan menurunkannya menjadi bentuk kata yang biasanya dipisahkan dengan menggunakan spasi.

Contoh dari URL Bahasa Inggris:

- jumptheshark.com => jump the shark .com

### Morphological Analysis

`Morfologi` adalah bidang linguistik yang mempelajari struktur internal dari kata-kata
`Morfem` satuan bahasa terkecil yang memiliki makna semantik.

Arti: membagi suatu kata ke dalam bentuk morfemnya.

Contoh:

- Carried $\Rightarrow$ carry + ed (past tense)
- Independently $\Rightarrow$ in + (depend + ent) + ly

### Part of Speech (POS Tagging)

Memberi anotasi pad setiap kata dalam kalimat dengan part-of-speech

| **I **  | **ate** | **the ** | **spaghetti** | **with** | **meatballs** |
| ------- | ------- | -------- | ------------- | -------- | ------------- |
| Pronoun | Verb    | Det      | Noun          | Prep     | Noun          |

![](image/20221113202911.png)

Bagian ini berguna untuk menganalisa sintaks dan diambuguasi arti kata

### Phrase Chunking

Suatu proses dalam NLP yang digunakan untuk mengidentifikasi part of speech dan menemukan Noun Phrases (NP) dan Verb Phrases (VB) yang bersifat non recursive

| **I ** | **ate** | **the spaghetti** | **with** | **meatballs** |
| ------ | ------- | ----------------- | -------- | ------------- |
| NP     | VP      | NP                | PP       | NP            |

### Syntatic Parsing

#### Context Free Grammar

Tata bahasa yang mempunyai tujuan sama seperti halnya tata bahasa reguler yaitu merupakan suatu cara untuk menunjukkan bagaimana menghasilkan suatu untai-untai dalam suatu bahasa.

#### Grammar

- S → NP VP
- S → Aux NP VP
- S → VP
- NP → Pronoun
- NP → Proper-Noun
- NP → Det Nominal
- Nominal → Noun
- Nominal → Nominal Noun
- Nominal → Nominal PP
- VP → Verb
- VP → Verb NP
- VP → VP PP
- PP → Prep NP

#### Lexicon

- Det → the | a | that | this
- Noun → book | flight | meal | money
- Verb → book | include | prefer
- Pronoun → I | he | she | me
- Proper-Noun → Houston | NWA
- Aux → does
- Prep → from | to | on | near | through

#### Sentece Generation

Kalimat dihasilkan dengan menulis ulang simbol awal secara rekursif menggunakan produksi sampai hanya simbil akhir yang tersisa.

#### Parsing

Mencari bentuk turunan dari string yang diberikan

- Top-Down Parsing: 
  - Mulai mencari bentuk turunan dimulai dari mengevaluasi parse tree dari atas dan bergerak kebawah untuk parsing node lainnya
- Down-Up Parsing: 
  - Mulai mencari bentuk turunan dengan terbalik, dimulai dari level terbawah dari tree dan bergerak ke atas untuk parsing node.

![](image/20221114002354.png)



## Semantic Tasks

### Word Sense Disambiguation (WSD)

Kata yang memiliki banyak arti biasanya memiliki sifat yang ambigu sehingga arti dari kata tersebut harus ditentukan berdasarkan kalimatnya.

### Semantic Role Labeling (SRL)

Semantic tasks yang digunakan oleh setiap noun phrase sehingga menjelaskan kata kerja pada setiap klausa

Disebut juga `*case role analysis*`, `*thematic analysis*`, dan `*shallow semantic parsing*`.

### Semantic Parsing

Memetakan kalimat bahasa alami ke representasi semantik yang lengkap dan terperinci. Dalam beberapa aplikasi, output yang diinginkan segera dieksekusi oleh program lain.

### Textual Entailment

Natural Language memerlukan kalimat yang lain dengan interpretasi biasa?

_Entailment_: hubungan antara dua kalimat bahwa jika yang pertama benar, maka yang kedua harus benar.

_Example_:

- Anak laki-laki pertamanya selalu mengantarkannya naik mobil ke tempat kerjanya setiap hari.
- Anak laki-laki pertamanya mengetahui bagaimana mengendarai mobil

## Pragmatics Tasks

### Anaphora Resolution / Co-Reference

Menentukan frase mana yang merujuk pada entitas yang sama

_Example:_

- John put the `carrot` on the plate and ate `it`

### Ellipsis Resolution

Menghilangkan sebagian kata atau frase karena dapat disimpulkan dari text tersebut.

_Example:_

- Elizabeth likes the Minnesota Vikings and her father, the Patriots.

Kalimat yang lengkap dari kalimat di atas adalah `Elizabeth likes the Minnesota Vikings and her father likes the Patriots.`

## Other Tasks

### Information Extraction (IE)

Identifikasi frase yang merujuk pada jenis entitas tertentu dan hubungan dalam teks

- Named Entity Recognition = Mengidentifikasi nama orang, tempat, organizations, places
  - Contoh:

| Michael Dell | is the CEO of | Dell Computer Corporation | and lives in | Austin Texas |
| ------------ | ------------- | ------------------------- | ------------ | ------------ |
| People       |               | Organization              |              | Places       |

- Relation Extraction = mengidentifikasi hubungan spesifik antar entitas.

### Question Answering

Menjawab pertanyaan Natural Language berdasarkan informasi yang disajikan pada suatu kumpulan tulisan.

*Example:*
  - When was Barack Obama born ? 
    - August 4, 1961

### Text Summarization

Ringkasan dari sebuah dokumen atau artikel yang lebih panjang.

### Machine Translation

Menerjemahkan suatu kalimat dari natural language ke bahasa lain

*Example:* Hasta la vista, bebé $\Rightarrow$ Until we see each other again, baby.

## NLP Task Problems

### Ambugity Resolusion untuk Translation

Ambuguitas sintaksis dan semantik harus diselesaikan dengan benar sehingga arti dari kalimat akan benar.

Pada awal sistem *machine translation* memberikan hasil yang kurang tepat ketika menerjemahkan Bahasa Inggris ke Rusia dan kemudian kembali ke Bahasa Inggris.
  - The spirit is willing but the flesh is weak.” $\Rightarrow$“The liquor is good but the meat is spoiled.

### Resolving Ambugity

Memiliki interpretasi yang benar dari kata *linguistic* dengan membutuhkan pengetahuan terkait yaitu, Syntax, Semantics, Pragmatics, dan World Knowledge

### Manual Knowledge Acquisition

Pemrosesan bahasa dimana membutuhkan spesialis manusia untuk menentukan dan memformalkan pengetahuan yang diperlukan. Pemrosesan ini bersifat manual, sulit, memakan waktu, dan rawan kesalahan.

*Rules* yang dibuat dalam suatu bahasa memiliki banyak pengecualian dan penyimpangan 

Sistem yang diikembangkan secara manual memerlukan cost mahal untuk dikembangkan dan kemampuan mereka terbatas dan *brittle*.

### Automatic Learning Approach

Menggunakan metode pembelajaran mesin untuk secara otomatis memperoleh pengetahuan yang diperlukan dari kumpulan tulisan teks yang dianotasi dengan tepat. Metode ini juga disebut sebagai pendekatan *corpus-based*, *statistical*, atau *empirical*.

![](image/20221114140050.png)  

#### Kelebihan Learning Approach

- Teks digital dalam jumlah besar sekarang tersedia
- memberikan keterangan pada kumpulan teks lebih mudah dan membutuhkan keahlian yang lebih sedikit.
- Algoritmanya telah berkembang untuk dapat menangani sejumlah besar data dan menghasiljan pengetahuan probabilistik yang akurat sehingga memungkinkan pemrosesan dapat menangani keteraturan linguistik.

### Pentingnya Probabilitas

Interpretasi kata dapat dikombinasikan untuk menghasilkan beberapa arti berbeda pada suatu kalimat.

Metode statistik memungkian menghitung interpretasi yang paling mungkin dengan menggabungkan interpretasi yang paling mungkin dengan menggabungkan bukti probabilistuk dari berbagai sumber yang tidak pasti.

