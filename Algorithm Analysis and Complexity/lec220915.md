---
title: Amortized Analysis 
author: Ronggo Tsani Musyafa
date: 15-09-2022 
---

# Multipop Stack

**Goal** Support operations on a set of elements:

- Push(S,x) : add element x to stack S
- Pop(S)    : remove and return the most recently added element
- Multi-Pop(S,k)    : remove the most recently added k element

**Theorema**. Mulai dari stack yang kosong, barisan campuran dari operasi Push, Pop, dan Multi-Pop operasi yang memakan waktu O(n^2) time.

**Proof**

- Menggunakan single Linked-list
- Push dan Pop memakan O(1) time masing^2
- Multi-Pop memakan O(n) waktu


## Multipop Stack: aggregate method

**Teorema**. Mulai dari stack yang kosong, barisan campuran dari operasi Push, Pop, dan Multi-Pop operasi yang memakan waktu O(n) time.

**Pf**

- Element di-pop maksimal satu kali setelah dipush
- Ada kurang dari sama dengan n Operasi Push
- Ada kurang dari sama dengan n Operasi Pop (termasuk yang dibuat dengan Multi-Pop)

## Multipop Stack: accounting method

**Credit**. 1 credit dibayar untuk push atau pop.
**Invariant**. Setiap element di stack mempunyai 1 credit.

**Accounting**

- Push(S,x) Charge 2 credit.
  - menggunakan 1 credit membayar untuk pushing x
  - menyimpan 1 credit untuk membayar popping x pada beberapa poin di masa depan
- Pop(S): Charge 0 credit
- MultiPop (S,k): Charge 0 credit

**Teorema**. Mulai dari stack yang kosong, barisan campuran dari operasi Push, Pop, dan Multi-Pop operasi yang memakan waktu O(n) time.

**Pf**

- Invariant : jumlah dari credit di struktur data lebih dari sama dengan 0
- Amortized cost per operasi tidak melebihi 2
- total cost sebenarnya dari n operasi kurang dari jumlah amortized cost.
