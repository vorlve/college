---
title: Assignment AAK
subtitle: Problem With Tree
author: Ronggo Tsani Musyafa -- `21/473988/PA/20449`
date: 02-11-2022
---

# Tower of Hanoi

Salah satu masalah yang menggunakan rekursi dan dapat menggunakan tree diagram untuk menyelesaikannya.
Goals dari masalah ini adalah memindahkan semua disk dari satu tiang ke tiang tujuan dengan syarat disk yang besar tidak boleh ada di atas disk yang lebih kecil.

```
tower(disk, source, destination, aux)

IF disk is equal 1, THEN
      move disk from source to destination
   ELSE
      tower(disk - 1, source, destination, aux)   // Step 1
      move disk from source to destination        // Step 2
      tower(disk - 1, aux, destination, source)   // Step 3
   END IF

END
```

![](image/20221103101923.png)

# Sum of Subset

Diberikan satu set elemen. Kita perlu mencari semua subset elemen yang jumlahnya sama dengan nilai penjumlahan / sum.

```cpp
bool isThereSubsetSum(int arr[], int n, int sum) {
  if (sum == 0)
    return true;
  if (n == 0)
    return false;

  if (arr[n - 1] > sum)
    return isThereSubsetSum(arr, n - 1, sum);

  return isThereSubsetSum(arr, n - 1, sum) ||
    isThereSubsetSum(arr, n - 1, sum - arr[n - 1]);
}
```

![](image/20221103123717.png)  

n=4, (w1 ,w2 ,w3 ,w4)=(3,4,5,6), W=13

![](image/20221103101614.png)

hasil subset = {3,4,6}
