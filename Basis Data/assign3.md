---
title: Assignment Basis Data
subtitle: ERD
author: Ronggo Tsani Musyafa -- `21/473988/PA/20449`
date: 23-10-2022
---

A. Gambar ER diagram pada Gambar 1 menggunakan tool ERDPlus.

![](image/image.png)

1. Sebutkan entitas dalam ERD dan jenis relationship (one-to-one, one-to-many, atau many-to-many) antar entitasnya.

   **Jawab:**

   - Entitas:
     - Band
     - Show
     - Instument
     - Repair Technician
   - Relationship:
     - Band to Show : Many to Many
     - Band to Instrument : One to Many
     - Instument to Repair Technician : Many to Many

2. Mengapa atribut ‘BandPhone’ ditempatkan dalam elip yang garisnya dobel?

   **Jawab:** Karena atribut `BandPhone` merupakan atribut yang bertipe Multi-Valued. Artibut `Bandphone` dapat memiliki lebih dari satu nilai.

3. Mengapa atribut ‘ShowID’ dibagi menjadi 2 atribut?

   **Jawab:** Karena atribut `ShowID` dapat dipecah menjadi 2 atribut yang lebih kecil. Dalam hal ini, `ShowID` dipecah menjadi ShowDate atau tanggal pertunjukan dan ShowVenue atau tempat pertunjukan sehingga atribut ShowID dapat menjadi unik. Hal ini dilakukan untuk memudahkan kita dalam menganalisis data Show yang ada.

B. Dari ER diagram yang anda buat di (1), konversikan menjadi skema basisdata.

![](image/skema.png)

1. Ada berapa tabel dalam skema basisdata yang dihasilkan?

   **Jawab:** Akan ada 8 tabel dalam skema basisdata,

2. Mengapa jumlah entitias dalam ERD tidak sama dengan jumlah tabel dalam skema basisdata?

   **Jawab:** Dalam relational entitas, terdapat relationship many to many. Hal itu menyebabkan adanya tabel penghubung antar dua entitas yang hanya berisi foreign keynya. Dan juga Atribut yang bertipe multivalue pun akan menjadi sebuah tabel juga.

C. Dari skema basisdata pada (2), konversikan menjadi SQL.

```sql
CREATE TABLE BAND
(
  BandName INT NOT NULL,
  BandID INT NOT NULL,
  BandAddress INT NOT NULL,
  BandContact INT NOT NULL,
  PRIMARY KEY (BandID),
  UNIQUE (BandName)
);

CREATE TABLE SHOW
(
  ShowName INT,
  ShowType INT NOT NULL,
  ShowVenue INT NOT NULL,
  ShowDate INT NOT NULL,
  PRIMARY KEY (ShowVenue, ShowDate)
);

CREATE TABLE INSTRUMENT
(
  InsYearMade INT NOT NULL,
  InsBrand INT NOT NULL,
  InsModel INT NOT NULL,
  InsSerialNo INT NOT NULL,
  BandID INT,
  PRIMARY KEY (InsSerialNo),
  FOREIGN KEY (BandID) REFERENCES BAND(BandID)
);

CREATE TABLE REPAIR_TECHNICIAN
(
  RTSSN INT NOT NULL,
  RTName INT NOT NULL,
  RTAddress INT NOT NULL,
  PRIMARY KEY (RTSSN)
);

CREATE TABLE PerformsIn
(
  BandID INT NOT NULL,
  ShowVenue INT NOT NULL,
  ShowDate INT NOT NULL,
  PRIMARY KEY (BandID, ShowVenue, ShowDate),
  FOREIGN KEY (BandID) REFERENCES BAND(BandID),
  FOREIGN KEY (ShowVenue, ShowDate) REFERENCES SHOW(ShowVenue, ShowDate)
);

CREATE TABLE Repairs
(
  RTSSN INT NOT NULL,
  InsSerialNo INT NOT NULL,
  PRIMARY KEY (RTSSN, InsSerialNo),
  FOREIGN KEY (RTSSN) REFERENCES REPAIR_TECHNICIAN(RTSSN),
  FOREIGN KEY (InsSerialNo) REFERENCES INSTRUMENT(InsSerialNo)
);

CREATE TABLE BAND_BandPhone
(
  BandPhone INT NOT NULL,
  BandID INT NOT NULL,
  PRIMARY KEY (BandPhone, BandID),
  FOREIGN KEY (BandID) REFERENCES BAND(BandID)
);

CREATE TABLE REPAIR_TECHNICIAN_RTPhone
(
  RTPhone INT NOT NULL,
  RTSSN INT NOT NULL,
  PRIMARY KEY (RTPhone, RTSSN),
  FOREIGN KEY (RTSSN) REFERENCES REPAIR_TECHNICIAN(RTSSN)
);
```

1. Apakah tipe atribut dalam SQL menurut Anda sudah sesuai untuk setiap atribut?

   **Jawab:** Ada beberapa atribut yang tipe datanya belum sesuai.

2. Jika ada yang belum sesuai, sarankan tipe atribut yang sesuai.

   **Jawab:**

   | Atribut     | Tipe hasil konversi | Tipe yang seharusnya |
   | ----------- | ------------------- | -------------------- |
   | ShowName    | INT                 | VARCHAR (25)         |
   | BandName    | INT                 | VARCHAR (25)         |
   | RTName      | INT                 | VARCHAR (25)         |
   | BandPhone   | INT                 | VARCHAR (25)         |
   | RTPhone     | INT                 | VARCHAR (25)         |
   | ShowDate    | INT                 | DATE                 |
   | ShowVenue   | INT                 | VARCHAR (25)         |
   | ShowType    | INT                 | VARCHAR (25)         |
   | BandAddress | INT                 | TEXT                 |
   | BandContact | INT                 | VARCHAR (25)         |
   | InsModel    | INT                 | VARCHAR (25)         |
   | InsBrand    | INT                 | VARCHAR (25)         |
