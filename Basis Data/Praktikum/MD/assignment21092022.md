---
title: Praktikum Database
subtitle: DML 2
author: Ronggo Tsani Musyafa
date: 19-09-2022
---
```sql
DROP DATABASE IF EXISTS school;

CREATE DATABASE school;

USE school;

CREATE TABLE student (
    No INT NOT NULL AUTO_INCREMENT,
    NIM INT NOT NULL,
    Name VARCHAR (20) NOT NULL,
    city_address VARCHAR (50) NOT NULL,
    age INT NOT NULL,
    ipk FLOAT NOT NULL,
    departement VARCHAR (20),
    PRIMARY KEY(No, NIM)
);

INSERT INTO student (NIM, Name, city_address, age, ipk, departement)
VALUES
(12345, 'Adi', 'Jakarta', 17, 2.5, 'Math'),
(12346, 'Ani', 'Yogyakarta', 20, 2.1, 'Math'),
(12347, 'Ari', 'Surabaya', 18, 2.5, 'Computer'),
(12348, 'Ali', 'Banjarmasin', 20, 3.5, 'Computer'),
(12349, 'Abi', 'Medan', 17, 3.7, 'Computer'),
(12350, 'Budi', 'Jakarta', 19, 3.8, 'Computer'),
(12351, 'Boni', 'Yogyakarta', 20, 3.2, 'Computer'),
(12352, 'Bobi', 'Surabaya', 17, 2.7, 'Computer'),
(12353, 'Beni', 'Banjarmasin', 18, 2.3, 'Computer'),
(12354, 'Cepi', 'Jakarta', 20, 2.2),
(12355, 'Coni', 'Yogyakarta', 22, 2.6),
(12356, 'Ceki', 'Surabaya', 21, 2.5, 'Math'),
(12357, 'Dodi', 'Jakarta', 20, 3.1, 'Math'),
(12358, 'Didi', 'Yogyakarta', 19, 3.2, 'Physics'),
(12359, 'Deri', 'Surabaya', 19, 3.3, 'Physics'),
(12360, 'Eli', 'Jakarta', 20, 2.9, 'Physics'),
(12361, 'Endah', 'Yogyakarta', 18, 2.8, 'Physics'),
(12362, 'Feni', 'Jakarta', 17, 2.7),
(12363, 'Farah', 'Yogyakarta', 18, 3.5),
(12364, 'Fandi', 'Surabaya', 19, 3.4);

-- Show Table Student
SELECT * FROM student;

-- Tampilkan kolom city address tanpa ada nilai duplikat
SELECT DISTINCT city_address FROM student;

-- Tampilkan data name dari mahasiswa yang memiliki departemen 'Computer'
SELECT Name FROM student WHERE departement = 'Computer';

-- Tampilkan data nim, name, age dan address yang diurutkan 
-- dari mahasiswa yang tertua
SELECT NIM, Name, age, city_address FROM student ORDER BY age DESC;

-- Tampilkan name dan age dari 3 mahasiswa termuda dari 'Jakarta'
SELECT Name, age FROM student WHERE city_address = 'Jakarta' ORDER BY
age LIMIT 3;

-- Tampilkan name dan ipk mahasiswa dari 'Jakarta' dengan ipk dibawah 2,5
SELECT Name, ipk FROM student WHERE city_address = 'Jakarta' and
ipk < 2.5;

-- Tampilkan name mahasiswa mana saja yang dari 'Yogyakarta'
-- atau berusia lebih dari 20 tahun
SELECT Name FROM student WHERE city_address =
'Yogyakarta' or age > 20;

-- Tampilkan name dan address mahasiswa yang bukan 
-- dari 'Jakarta' dan 'Surabaya'
SELECT Name, city_address FROM student WHERE city_address NOT IN
('Jakarta', 'Surabaya');

-- Tampilkan name, age dan ipk mahasiswa dengan ipk dari 2,5 hingga 3,5
SELECT Name, age, ipk FROM student WHERE ipk < 3.5 and ipk > 2.5;

--Tunjukkan name mahasiswa yang memiliki huruf a pada namanya
SELECT Name FROM student WHERE Name like '%a%';

-- Tampilkan NIM mahasiswa dengan data NULL
SELECT NIM FROM student WHERE departement IS NULL;
```

![](image/20220921132228.png)
![](image/20220921132510.png)

1. Tampilkan kolom city address tanpa ada nilai duplikat

   ![](image/20220921132540.png)

2. Tampilkan data name dari mahasiswa yang memiliki departemen 'Computer'

   ![](image/20220921132642.png)

3. Tampilkan data nim, name, age dan address yang diurutkan dari mahasiswa yang tertua

   ![](image/20220921132826.png)

4. Tampilkan name dan age dari 3 mahasiswa termuda dari 'Jakarta'

   ![](image/20220921132900.png)

5. Tampilkan name dan ipk mahasiswa dari 'Jakarta' dengan ipk dibawah 2,5

   ![](image/20220921132948.png)

6. Tampilkan name mahasiswa mana saja yang dari 'Yogyakarta' atau berusia lebih dari 20 tahun

   ![](image/20220921133400.png)

7. Tampilkan name dan address mahasiswa yang bukan dari 'Jakarta' dan 'Surabaya'

   ![](image/20220921133443.png)

8. Tampilkan name, age dan ipk mahasiswa dengan ipk dari 2,5 hingga 3,5

   ![](image/20220921133737.png)

9. Tunjukkan name mahasiswa yang memiliki huruf a pada namanya

   ![](image/20220921133756.png)

10. Tampilkan NIM mahasiswa dengan data NULL

    ![](image/20220921133828.png)
