---
title: Assignment Prak Database
subtitle: Function and Trigger
author: Ronggo Tsani Musyafa -- `21/473988/PA/20449`
date: 09-11-2022 
---

# Query

```sql
/* Buatlah suatu fungsi untuk mengkonversi price 
dalam database classicmodels ke mata uang baru, 
yang akan dihitung dengan: price * 14560 */

CREATE FUNCTION ConvertPrice(price DOUBLE) RETURNS DOUBLE
DETERMINISTIC
BEGIN
    SET price = price * 14560;
    RETURN (price);
END $$

SELECT buyPrice, ConvertPrice(buyPrice)
FROM products
LIMIT 5;

/*Buatlah trigger untuk mengurangi stok suatu produk secara 
otomatis ketika ada penambahan pada tabel order_details*/

SELECT quantityInStock
FROM products
WHERE productCode = 'S10_1949';

CREATE TRIGGER after_orderdetails_insert
AFTER INSERT ON orderdetails
FOR EACH ROW
    BEGIN
        UPDATE products
        SET quantityInStock = quantityInStock - NEW.quantityOrdered
        WHERE productCode = NEW.productCode;
    END $$

INSERT orderdetails(orderNumber, productCode, 
                    quantityOrdered, priceEach, 
                    orderLineNumber)
    VALUES (10421, 'S10_1949', 20, '121.64', 4);

SELECT quantityInStock
FROM products
WHERE productCode = 'S10_1949';
        

/*Buatlah sebuah function untuk mengetahui 
 harga grosir yang disesuaikan dengan jumlah 
 barang yang akan dibeli. Semisal barang akan 
 dapat diskon 10% apabila beli minimal 1 lusin, 
 dan diskon 15% apabila beli minimal 100. */

CREATE FUNCTION bulk_price(p_Price FLOAT, p_quantityOrdered INT) 
RETURNS FLOAT
DETERMINISTIC
BEGIN
    -- SET p_Price = p_Price * p_quantityOrdered;
    IF p_quantityOrdered >= 12 THEN
        SET p_Price = p_Price * 0.9;
    ELSEIF p_quantityOrdered >= 100 THEN
        SET p_Price = p_Price * 0.85;
    END IF;
    RETURN (p_Price);
END $$

SELECT orders.customerNumber,priceEach, 
        bulk_price(priceEach, quantityOrdered) AS bulkPrice
FROM orderdetails
JOIN orders
ON orders.orderNumber = orderdetails.orderNumber
LIMIT 5;



/* Buatlah sebuah trigger untuk merekam history 
perubahan product serta buatlah tabel baru untuk 
menyimpan perubahan tersebut. */

CREATE TABLE products_audit (
    idAudit INT PRIMARY KEY AUTO_INCREMENT,
    productName varchar(70) NOT NULL,
    productLine varchar(50) NOT NULL,
    productScale varchar(10) NOT NULL,
    productVendor varchar(50) NOT NULL,
    productDescription text NOT NULL,
    quantityInStock smallint(6) NOT NULL,
    buyPrice decimal(10,2) NOT NULL,
    MSRP decimal(10,2) NOT NULL,
    changedat DATE DEFAULT NULL,
    ACTION VARCHAR(50) DEFAULT NULL
);

CREATE TRIGGER before_products_update
    BEFORE UPDATE ON products
    FOR EACH ROW
BEGIN 
    INSERT INTO products_audit
    SET ACTION = 'UPDATE',
    productName = OLD.productName,
    productLine = OLD.productLine,
    productScale = OLD.productScale,
    productVendor = OLD.productVendor,
    productDescription= OLD.productDescription,
    quantityInStock = OLD.quantityInStock,
    buyPrice = OLD.buyPrice,
    MSRP = OLD.MSRP,
    changedat = NOW();
END $$

CREATE TRIGGER before_products_delete
BEFORE DELETE ON products
FOR EACH ROW
    BEGIN 
        INSERT INTO products_audit
        SET ACTION = 'DELETE',
        productName = OLD.productName,
        productLine = OLD.productLine,
        productScale = OLD.productScale,
        productVendor = OLD.productVendor,
        productDescription= OLD.productDescription,
        quantityInStock = OLD.quantityInStock,
        buyPrice = OLD.buyPrice,
        MSRP = OLD.MSRP,
        changedat = NOW();
    END $$

CREATE TRIGGER after_products_insert
AFTER INSERT ON products
FOR EACH ROW
    BEGIN 
        INSERT INTO products_audit
        SET ACTION = 'INSERT',
        productName = NEW.productName,
        productLine = NEW.productLine,
        productScale = NEW.productScale,
        productVendor = NEW.productVendor,
        productDescription= NEW.productDescription,
        quantityInStock = NEW.quantityInStock,
        buyPrice = NEW.buyPrice,
        MSRP = NEW.MSRP,
        changedat = NOW();
    END $$

DELIMITER ;
UPDATE products
SET quantityInStock = 7305
WHERE productCode = 'S10_1949';

SELECT * FROM products_audit;
```
# Output

1. ![](image/20221109135422.png)  
2. ![](image/20221109135443.png)  ![](image/20221109135456.png)  
3. ![](image/20221109135511.png)  
4. ![](image/20221109135546.png)  