---
title: Assignment Praktikum Database
subtitle: Procedure
author: Ronggo Tsani Musyafa -- `21/473988/PA/20449`
date: 02-11-2022 
---
# Query

```sql
USE classicmodels;
DELIMITER $$

CREATE PROCEDURE IncreasePrice (
    IN categoryName VARCHAR(255),
    IN percent INT
)
BEGIN
    UPDATE products
    SET MSRP = (MSRP * ((percent * 0.01)+ 1))
    WHERE productLine = categoryName;
END $$

CREATE PROCEDURE CustOrderTotSpesifik(
    IN orderMonth INT,
    IN orderYear INT,
    IN nameChar VARCHAR(10),
    OUT total INT
)
BEGIN
    SELECT SUM(orderdetails.quantityOrdered) INTO total
    FROM orders
    JOIN customers
    ON customers.customerNumber = orders.customerNumber
    JOIN orderdetails
    ON orders.orderNumber = orderdetails.orderNumber
    WHERE MONTH(orderDate) = orderMonth
    AND YEAR(orderDate) = orderYear
    AND customerName LIKE CONCAT('%', nameChar, '%');
END $$

CREATE PROCEDURE ChangeCredLimit(
    IN customerCountry VARCHAR(50),
    IN percent INT
)
BEGIN 
    UPDATE customers
    SET creditLimit = creditLimit * ((percent * 0.01) + 1)
    WHERE country = customerCountry;
END $$
DELIMITER ;
```

\pagebreak

# Question

1. Buatlah sebuah prosedur untuk menaikkan harga kategori produk
tertentu dengan persentase tertentu. Buatlah tabel product dengan
data yang sesuai untuk menguji prosedur yang telah dibuat. Atau,
load database classicmodels pada personal machine sehingga dapat
memiliki akses lengkap. DELIMITER harus diubah sebelum membuat
prosedur.

![](image/20221102135523.png)  

2. Buatlah prosedur untuk melaporkan jumlah yang dipesan pada bulan dan
tahun tertentu untuk customer yang memiliki string karakter tertentu
pada nama mereka.
   
![](image/20221102140740.png)  

3. Buatlah prosedur untuk mengubah batas kredit seluruh *customer* di
negara tertentu dengan persentase tertentu.

![](image/20221102140344.png)  
![](image/20221102140357.png)  