---
title: Assignment Praktikum Basis Data
subtitle: JOIN 2
author: Ronggo Tsani Musyafa -- `21/473988/PA/20449`
date: 24-10-2022
---

## Query SQL

```sql
DROP DATABASE IF EXISTS library;

CREATE DATABASE library;

USE library;

CREATE TABLE Books(
    bookID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    bookTitle TEXT,
    authorName VARCHAR(50),
    borrowedStatus BOOLEAN
);
-- borrowedStatus = 0 => dipinjam
-- borrowedStatus = 1 => tersedia

CREATE TABLE Users (
    userID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    userName VARCHAR (50),
    numberOfBorrowing INT,
    numberOfReturning INT
);

ALTER TABLE Books AUTO_INCREMENT=1001;

CREATE TABLE Flow(
    flowID INT NOT NULL AUTO_INCREMENT,
    userIDBorrowing INT NOT NULL,
    bookIDBorrowed INT NOT NULL,
    borrowedDate DATE,
    returnDate DATE,
    PRIMARY key (flowID),
    FOREIGN KEY (userIDBorrowing) REFERENCES Users(userID),
    FOREIGN KEY (bookIDBorrowed) REFERENCES Books(bookID)
);

INSERT INTO Books(bookTitle, authorName, borrowedStatus)
VALUES
    ("Pulang", "Tere Liye", 1),
    ("Pergi", "Tere Liye", 0),
    ("Pulang-Pergi", "Tere Liye", 0),
    ("Bumi", "Tere Liye", 1),
    ("Bulan", "Tere Liye", 0),
    ("Matahari", "Tere Liye", 0),
    ("Komet", "Tere Liye", 1),
    ("Bintang", "Tere Liye", 1),
    ("Ceros dan Betozar", "Tere Liye", 1),
    ("Laut Bercerita", "Leila S. Chudori", 0),
    ("Atomic Habits", "James Clear", 0),
    ("The Subtle Art of Not Giving a F*ck", "Mark Manson", 0),
    ("One Piece 99", "Eiichiro Oda", 1),
    ("Cantik Itu Luka", "Eka Kurniawan", 1),
    ("Merawat Luka Batin", "dr. Jiemi Ardian, Sp.KJ", 0),
    ("Home Sweet Loan", "Almira Bastari", 1),
    ("How to Win Friends and Influence People in Digital Age",
        "Dale Carnegie & Associates", 0),
    ("Masih Ingatkah Kau Jalan Pulang",
        "Sapardi Djoko Damono & Rintik Sedu", 0),
    ("Jakarta Sebelum Pagi", "Ziggy Zeszyazeoviennazabrizkie", 1),
    ("Layangan Putus", "Mommy ASF", 1);

INSERT INTO Users(userName, numberOfBorrowing, numberOfReturning)
VALUES
    ("Billy", 1, 1),
    ("Ronggo", 2, 2),
    ("Faiz", 0, 2),
    ("Rachel", 1, 3),
    ("Ninda", 1, 0),
    ("Irfan", 0, 1),
    ("Rey", 0, 0),
    ("Farrel", 0, 2),
    ("Putri", 0, 3),
    ("Maharani", 1, 5),
    ("Ferdi", 1, 3),
    ("Aziz", 1, 6),
    ("Abid", 2, 1);

INSERT INTO Flow(userIDBorrowing, bookIDBorrowed, borrowedDate, returnDate)
VALUES
    (4, 1012, DATE_SUB(CURRENT_DATE(), INTERVAL 12 DAY),
        DATE_ADD(CURRENT_DATE(), INTERVAL 2 DAY)),
    (12, 1010, DATE_SUB(CURRENT_DATE(), INTERVAL 11 DAY),
        DATE_ADD(CURRENT_DATE(), INTERVAL 3 DAY)),
    (2, 1011, DATE_SUB(CURRENT_DATE(), INTERVAL 11 DAY),
        DATE_ADD(CURRENT_DATE(), INTERVAL 3 DAY)),
    (11, 1005, DATE_SUB(CURRENT_DATE(), INTERVAL 10 DAY),
        DATE_ADD(CURRENT_DATE(), INTERVAL 4 DAY)),
    (5, 1015, DATE_SUB(CURRENT_DATE(), INTERVAL 10 DAY),
        DATE_ADD(CURRENT_DATE(), INTERVAL 4 DAY)),
    (1, 1018, DATE_SUB(CURRENT_DATE(), INTERVAL 8 DAY),
        DATE_ADD(CURRENT_DATE(), INTERVAL 6 DAY)),
    (10, 1003, DATE_SUB(CURRENT_DATE(), INTERVAL 8 DAY),
        DATE_ADD(CURRENT_DATE(), INTERVAL 6 DAY)),
    (13, 1006, DATE_SUB(CURRENT_DATE(), INTERVAL 8 DAY),
        DATE_ADD(CURRENT_DATE(), INTERVAL 6 DAY)),
    (2, 1002, DATE_SUB(CURRENT_DATE(), INTERVAL 1 DAY),
        DATE_ADD(CURRENT_DATE(), INTERVAL 13 DAY)),
    (13, 1017, DATE_SUB(CURRENT_DATE(), INTERVAL 1 DAY),
        DATE_ADD(CURRENT_DATE(), INTERVAL 13 DAY));

-- Menampilkan semua judul buku yang memiliki
-- status dipinjam dan tanggal peminjamannya kemarin

SELECT bookTitle, borrowedDate FROM Books as b
JOIN Flow as f ON b.bookID = f.bookIDBorrowed
WHERE b.borrowedStatus = 0 AND
      f.borrowedDate = DATE_SUB(CURRENT_DATE(), INTERVAL 1 DAY);

-- Menampilkan semua judul buku, termasuk buku yang tidak
-- dipinjam dan userID peminjam yang meminjam buku tersebut

SELECT bookTitle, f.userIDBorrowing FROM Books as b
LEFT JOIN Flow as f ON b.bookID = f.bookIDBorrowed;

-- Menampilkan semua buku yang dipinjam dan semua userID,
-- baik dia meminjam atau tidak

SELECT  Users.userID, Books.bookTitle
FROM Flow
LEFT JOIN Books ON Flow.bookIDBorrowed = Books.bookID
RIGHT JOIN Users ON Users.userID = Flow.userIDBorrowing;

-- Menggunakan satu query, buatlah daftar semua judul
-- buku dan nama user yang meminjam buku tersebut dan
-- user tersebut telah meminjam lebih dari 3 buku.

SELECT Books.bookTitle, Users.userName FROM Users
JOIN Flow ON Flow.userIDBorrowing = Users.userID
RIGHT JOIN Books ON Flow.bookIDBorrowed = Books.bookID
WHERE (numberOfBorrowing + numberOfReturning) > 3;
```

## Output

1. ![](image/20221024204236.png)
2. ![](image/20221024204251.png)
3. ![](image/20221024205253.png)   
4. ![](image/20221024205319.png)  

