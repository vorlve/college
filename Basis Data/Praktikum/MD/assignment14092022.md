---
title: Praktikum Database
subtitle: PVFC Database
author: Ronggo Tsani Musyafa
date: 19-09-2022 
---

## Membuat Database dengan 5 baris data setiap tabel nya.

### Query yang digunakan dalam membuat database company.

```sql
    DROP DATABASE IF EXISTS company;

    CREATE DATABASE company;

    USE company;

    CREATE TABLE Customer (
        id INT NOT NULL AUTO_INCREMENT,
        name VARCHAR (100) NOT NULL,
        address VARCHAR (100) NOT NULL,
        PRIMARY KEY(id)
    );

    CREATE TABLE Transaction (
        id INT NOT NULL AUTO_INCREMENT,
        order_date date NOT NULL,
        customer_id INT NOT NULL,
        PRIMARY KEY (id),
        FOREIGN KEY (customer_id) REFERENCES Customer (id)
    );

    CREATE TABLE Product (
        id INT NOT NULL AUTO_INCREMENT,
        description TEXT NOT NULL,
        finish TEXT,
        quantity INT NOT NULL,
        unit_price INT NOT NULL,
        order_id INT NOT NULL,
        PRIMARY KEY(id),
        FOREIGN KEY(order_id) REFERENCES Transaction(id)
    );

    INSERT INTO Customer(id, name, address)
    VALUES 
        (1, 'Contemporary Casuals', '1355 S Hines Blvd'),
        (2, 'Value Furniture', '15145 S.W. 17th St.'),
        (3, 'Home Furnishings', '1900 Allard Ave.'),
        (4, 'Eastern Furniture', '1925 Beltline Rd.'),
        (5, 'Impressions', '5585 Westcott Ct.');

    INSERT INTO Transaction(id, order_date, customer_id)
    VALUES
    (1001, '2020-10-10', 1),
    (1002, '2020-11-18', 4),
    (1004, '2021-01-05', 5),
    (1005, '2021-01-30', 3),
    (1006, '2021-05-01', 2);

    INSERT INTO Product(id, description, finish, quantity, unit_price, order_id)
    VALUES
    (1, 'End Table', 'Cherry', 2, 175, 1001),
    (2, 'Coffee Table', 'Natural Ash', 3,  200, 1002),
    (3, 'Computer Desk', 'Natural Ash', 5, 375, 1004),
    (4, 'Entertainment Center', 'Natural Maple', 4, 650, 1006),
    (5, 'Writers Desk', 'Cherry', 10, 325, 1005);
```

### Outcome yang dihasilkan.

![](image/20220919001118.png)  
    