---
title: Assignment Praktikum Basis Data
subtitle: Join
author: Ronggo Tsani Musyafa -- `21/473988/PA/20449`
date: 19-10-2022 
---
```sql
DROP DATABASE IF EXISTS bookStore;
CREATE DATABASE bookStore;
USE bookStore;

CREATE TABLE Customer
(
    id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR (100) NOT NULL,
	address VARCHAR (100) NOT NULL,
    city VARCHAR (100) NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE Orders
(
    id INT NOT NULL AUTO_INCREMENT,
	order_date date NOT NULL,
	customer_id INT NOT NULL,
	PRIMARY KEY(id),
	FOREIGN KEY (customer_id) REFERENCES Customer(id)
);

CREATE TABLE Product
(
    id VARCHAR(10) NOT NULL,
    description TEXT NOT NULL,
    quantity INT NOT NULL,
    unit_price FLOAT NOT NULL,
    order_id INT NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY(order_id) REFERENCES Orders(id)
);

INSERT INTO Customer
    (id, name, address, city)
VALUES
    (1, 'Budi', '1355 S Hines Blvd', "Bandung, Indonesia"),
    (2, 'Hartono', '15145 S.W. 17th St.', 'California, USA'),
    (3, 'Budiman', '1900 Allard Ave.', 'New York, USA'),
    (4, 'Justin', '1925 Beltline Rd.', 'Bangkok, Thailand'),
    (5, 'Otsuka', '5585 Westcott Ct.', 'Tokyo, Japan')
;

INSERT INTO Orders
    (id, order_date, customer_id)
VALUES
    (1001, '1998-05-10', 1),
    (1222, '1998-05-18', 4),
    (1213, '1999-05-05', 5),
    (1314, '2000-01-30', 3),
    (1115, '2001-02-03', 2)
;

INSERT INTO Product
    (id, description, quantity, unit_price, order_id)
VALUES
    ("A123", 'Pencil', 20, 3, 1001),
    ("B234", 'Eraser', 30, 3.5, 1222),
    ('C345', 'Sharpener', 15, 4, 1213),
    ('E145', 'Ruler', 44, 3, 1314),
    ('D256', 'Correction Pen', 100, 5, 1115)
;


SELECT o.id, c.name, p.description, p.quantity
FROM Orders as o
    JOIN Product as p
    ON o.id = p.order_id
    JOIN Customer as c
    ON o.customer_id = c.id
WHERE year(o.order_date) = '1998' AND month(o.order_date) = '05';

SELECT c.name
FROM Orders as o
    JOIN Product as p
    ON o.id = p.order_id
    JOIN Customer as c
    ON o.customer_id = c.id
WHERE c.city LIKE '%USA' AND p.description IN ('Pencil', 'Ruler', 'Book');

SELECT o.id, c.name, c.address
FROM Orders as o
    JOIN Customer as c
    ON o.customer_id = c.id
WHERE o.id LIKE '12%';

SELECT p.description
FROM Orders as o
    JOIN Product as p
    ON o.id = p.order_id
ORDER BY p.quantity ASC;

SELECT o.order_date, c.name, c.city, o.id
FROM Orders as o
    JOIN Product as p
    ON o.id = p.order_id
    JOIN Customer as c
    ON o.customer_id = c.id
ORDER BY o.order_date DESC;
```



1. Menampilkan data nomer pesanan ,nama pemesan dan nama produk yang dipesan beserta jumlahnya untuk pemesanan yang dilakukan pada bulan Mei 1998'
   
   ![](image/20221019134216.png)  

2. Menampilkan Nama pemesan yang tinggal di USA dan memesan Pencil, Ruler atau Book
   
   ![](image/20221019134237.png)  

3. Menampilkan data order (nomer order, nama pemesan dan alamatnya) yang dua digit awal nomer ordernya adalah 12 .
   
   ![](image/20221019134256.png)  

4. Menampilkan nama semua barang yang dipesan customer dan diurutkan berdasarkan jumlah pesanan per barangnya (kecil-besar).
   
   ![](image/20221019134317.png)  

5. Menampilkan data yang terdiri dari : Tanggal Pesan, Nama Customer, Negara Customer- Nomer Pesan . Urutkan berdasarkan tanggal nya , dari pesanan yang paling baru
   
   ![](image/20221019134404.png)  