---
title: Assignment 2 Praktikum Basis Data
subtitile: DML
author: Ronggo Tsani Musyafa
date: 14-09-2022 
---

## Tuliskan query untuk membuat database store

![](image/20220914135610.png)  

## Tuliskan query untuk membuat tabel Customer, tabel Product dan Tabel Transaction, dengan struktur sebagai berikut

![](image/20220914135655.png) 

![](image/20220914135738.png)  

![](image/20220914135938.png)  

## Tuliskan query untuk memasukkan data pada ketiga tabel sesuai dengan data berikut (boleh langsung ditambahkan beberapa baris data sekaligus)

![](image/20220914140023.png)  

![](image/20220914140051.png)  

![](image/20220914140154.png)  

## Tampilkan nama city dari Customer secara unik

![](image/20220914140216.png)  

## Tampilkan seluruh data Customer

![](image/20220914140318.png)  

## Tampilkan productName dan stock dari seluruh data product

![](image/20220914140337.png)  

## Tampilkan customerName yang tinggal di kota Bandung

![](image/20220914140358.png)  

## Tampilkan transactionDate yang dilakukan oleh customer yang memiliki customerNumber C-007

![](image/20220914140414.png)  

## Tampilkan seluruh data product yang memiliki stock 15

![](image/20220914140432.png)  

## Ubahlah transactionDate dari customer yang memiliki customerNumber C-007 menjadi tanggal hari ini

![](image/20220914140458.png)  

## Ubahlah customerName menjadi HappyFamily Mart dan city menjadi Jakarta untuk customer yang memiliki customerNumber C-009

![](image/20220914140516.png)  

## Tambahkan data product berikut menggunakan nilai DEFAULT

![](image/20220914140535.png)  

## Tambahkan data transaction berikut menggunakan tanggal hari ini pada transactionDate

![](image/20220914140653.png)  

## Hapuslah 2 data transaction pertama yang diurutkan berdasarkan transactionNumber  
## Hapuslah 2 data transaction terakhir yang diurutkan berdasarkan transactionNumber

![](image/20220914140708.png)  

