---
title: Praktikum Basis Data
subtitle: Aggregate Function -- Task 2
author: Ronggo Tsani Musyafa -- `21/473988/PA/20449`
date: 28-09-2022 
---

1. Tampilkan kolom city address tanpa duplikat

    ![](image/20220928140010.png)  

2. Tampilkan ipk maksimum mahasiswa dari Jakarta

    ![](image/20220928140059.png)  

3. Tampilkan ipk minimum dari mahasiswa jurusan Komputer

    ![](image/20220928140025.png)  

4. Tampilkan jumlah mahasiswa di departemen Komputer

    ![](image/20220928140128.png)  

5. Tampilkan IPK rata-rata dari mahasiswa jurusan fisika

    ![](image/20220928140315.png)  

6. Tampilkan jumlah data mahasiswa di setiap jurusan

    ![](image/20220928140514.png)  

7. Tampilkan jumlah data mahasiswa dari city yang berbeda

    ![](image/20220928140532.png)  

8. Tampilkan data jumlah kelompok mahasiswa berdasarkan alamat kota dengan usia dibawah 19 tahun

    ![](image/20220928140559.png)  