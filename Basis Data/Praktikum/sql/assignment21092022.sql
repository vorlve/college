DROP DATABASE IF EXISTS school;

CREATE DATABASE school;

USE school;

CREATE TABLE student
(
    No INT NOT NULL AUTO_INCREMENT,
    NIM INT NOT NULL,
    student_name VARCHAR (20) NOT NULL,
    city_address VARCHAR (50) NOT NULL,
    age INT NOT NULL,
    ipk FLOAT NOT NULL,
    departement VARCHAR (20),
    PRIMARY KEY (No, NIM)
);

    INSERT INTO student
        (NIM, student_name, city_address, age, ipk, departement)
    VALUES
        (12345, 'Adi', 'Jakarta', 17, 2.5, 'Math'),
        (12346, 'Ani', 'Yogyakarta', 20, 2.1, 'Math'),
        (12347, 'Ari', 'Surabaya', 18, 2.5, 'Computer'),
        (12348, 'Ali', 'Banjarmasin', 20, 3.5, 'Computer'),
        (12349, 'Abi', 'Medan', 17, 3.7, 'Computer'),
        (12350, 'Budi', 'Jakarta', 19, 3.8, 'Computer'),
        (12351, 'Boni', 'Yogyakarta', 20, 3.2, 'Computer'),
        (12352, 'Bobi', 'Surabaya', 17, 2.7, 'Computer'),
        (12353, 'Beni', 'Banjarmasin', 18, 2.3, 'Computer'),
        (12354, 'Cepi', 'Jakarta', 20, 2.2, NULL),
        (12355, 'Coni', 'Yogyakarta', 22, 2.6, NULL),
        (12356, 'Ceki', 'Surabaya', 21, 2.5, 'Math'),
        (12357, 'Dodi', 'Jakarta', 20, 3.1, 'Math'),
        (12358, 'Didi', 'Yogyakarta', 19, 3.2, 'Physics'),
        (12359, 'Deri', 'Surabaya', 19, 3.3, 'Physics'),
        (12360, 'Eli', 'Jakarta', 20, 2.9, 'Physics'),
        (12361, 'Endah', 'Yogyakarta', 18, 2.8, 'Physics'),
        (12362, 'Feni', 'Jakarta', 17, 2.7, NULL),
        (12363, 'Farah', 'Yogyakarta', 18, 3.5, NULL),
        (12364, 'Fandi', 'Surabaya', 19, 3.4, NULL);

    SELECT *
    FROM student;
    
    SELECT DISTINCT city_address
    FROM student;

    SELECT student_name
    FROM student
    WHERE departement = 'Computer';

    SELECT NIM, student_name, age, city_address
    FROM student
    ORDER BY age DESC;

    SELECT student_name, age
    FROM student
    WHERE city_address = 'Jakarta'
    ORDER BY age LIMIT 3;

    SELECT student_name, ipk 
    FROM student 
    WHERE city_address = 'Jakarta' and ipk < 2.5;

    SELECT student_name
    FROM student
    WHERE city_address = 'Yogyakarta' or age > 20;

    SELECT student_name, city_address
    FROM student
    WHERE city_address NOT IN ('Jakarta', 'Surabaya');

    SELECT student_name, age, ipk
    FROM student
    WHERE ipk > 2.5 AND ipk < 3.5
    ORDER BY ipk;

    SELECT student_name
    FROM student
    WHERE student_name like '%a%';

    SELECT NIM
    FROM student
    WHERE departement IS NULL;