DROP DATABASE IF EXISTS libraries;

CREATE DATABASE libraries;

USE libraries;

CREATE TABLE book (
    bookID INT NOT NULL,
    bookTitle VARCHAR(255) NOT NULL,
    authorName CHAR(255) NOT NULL, 
    borrowedStatus CHAR(255) NOT NULL,
    PRIMARY KEY (bookID)
);

CREATE TABLE user (
    userID INT NOT NULL, 
    userName VARCHAR(255) NOT NULL,
    numberOfBorrowing INT NOT NULL,
    numberOfReturning INT NOT NULL,
    PRIMARY KEY (userID)
);

CREATE TABLE flow (
    flowID INT NOT NULL,
    userIDBorrowing INT NOT NULL,
    bookIDBorrowed INT NOT NULL,
    borrowDate DATE,
    returnDate DATE,
    PRIMARY KEY (flowID),
    FOREIGN KEY (userIDBorrowing) REFERENCES user(userID),
    FOREIGN KEY (bookIDBorrowed) REFERENCES  book(bookID)
);

INSERT INTO book(bookID, bookTitle, authorName, borrowedStatus)
VALUES
(202201, 'WHY?', 'Kwang Woong Lee', 'Borrowed'),
(202202, 'Basis Data Dasar', 'Adyanata Lubis', 'Ready'),
(202203, 'Calculus', 'Edwin Purcell', 'Ready'),
(202204, 'The Psychology of Money', 'Morgan Housel', 'Borrowed'),
(202205, 'The Intelligent Investor', 'Benjamin Graham', 'Borrowed');

INSERT INTO user(userID, userName, numberOfBorrowing, numberOfReturning)
VALUES
(470255, 'Wahyu', 7, 6),
(470256, 'Rama', 4, 2),
(470257, 'Awan', 1, 1),
(470258, 'Angkasa', 3, 2),
(470259, 'Pluto', 5, 5);

INSERT INTO flow(flowID, userIDBorrowing, bookIDBorrowed, borrowDate, returnDate)
VALUES
(31, 470255, 202201, '2022-10-22', null),
(32, 470256, 202202, '2022-10-10','2022-10-12'),
(33, 470257, 202203, '2022-09-29','2022-10-09'),
(34, 470258, 202204, '2022-10-19', null),
(35, 470259, 202205, '2022-09-29', null);
