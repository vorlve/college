USE classicmodels; 
DROP PROCEDURE IF EXISTS GetAllProducts;
DROP PROCEDURE IF EXISTS GetOfficeByCountry;
DROP PROCEDURE IF EXISTS CountOrderByStatus;
DROP PROCEDURE IF EXISTS GetCustomerLevel;
DROP PROCEDURE IF EXISTS GetCustomerShipping;
DROP PROCEDURE IF EXISTS IncreasePrice;
DROP PROCEDURE IF EXISTS CustOrderTotSpesifik;
DROP PROCEDURE IF EXISTS set_counter;
DROP PROCEDURE IF EXISTS ChangeCredLimit;


DELIMITER $$

CREATE PROCEDURE GetAllProducts()
BEGIN
    SELECT * FROM products;
END $$

CREATE PROCEDURE GetOfficeByCountry(IN countryName VARCHAR(255))
BEGIN  
    SELECT *
    FROM offices
    WHERE country = countryName;
END $$

CREATE PROCEDURE CountOrderByStatus(
    IN orderStatus VARCHAR(255),
    OUT total INT
)
BEGIN
    SELECT COUNT(orderStatus) INTO total
    FROM orders
    WHERE status = orderStatus;
END $$

CREATE PROCEDURE set_counter(
    INOUT counter INT(4),
    IN inc INT(4)
)
BEGIN 
    SET counter = counter + inc;
END $$

CREATE PROCEDURE GetCustomerLevel(
    IN p_customerNumber INT,
    OUT p_customerLevel VARCHAR(10)
)
BEGIN 
    DECLARE creditLim DOUBLE;
    SELECT creditlimit INTO creditLim
    FROM customers
    WHERE customerNumber = p_customerNumber;
    IF creditLim > 50000 THEN
        SET p_customerLevel = 'Platinum';
    ELSEIF ( creditLim <= 50000 AND creditLim >= 10000 ) THEN 
        SET p_customerLevel = 'Gold';
    ELSEIF ( creditLim <= 10000 ) THEN 
        SET p_customerLevel = 'Silver';
    END IF;
END $$
CREATE PROCEDURE GetCustomerShipping(
    IN p_customerNumber INT,
    OUT p_shipping VARCHAR(50)
)
BEGIN 
    DECLARE customerCountry VARCHAR (50);
    SELECT country INTO customerCountry
    FROM customers
    WHERE customerNumber = p_customerNumber;
    CASE customerCountry
    WHEN 'USA' THEN
        SET p_shipping = '2-day Shipping';
    WHEN 'Canada' THEN 
        SET p_shipping = '3-day Shipping';
    ELSE
        SET p_shipping = '5-day Shipping';
    END CASE;
END $$

CREATE PROCEDURE IncreasePrice (
    IN categoryName VARCHAR(255),
    IN percent INT
)
BEGIN
    UPDATE products
    SET MSRP = (MSRP * ((percent * 0.01)+ 1))
    WHERE productLine = categoryName;
END $$
/*
2. Buatlah prosedur untuk melaporkan jumlah yang dipesan pada bulan dan
tahun tertentu untuk customer yang memiliki string karakter tertentu
pada nama mereka.
*/

CREATE PROCEDURE CustOrderTotSpesifik(
    IN orderMonth INT,
    IN orderYear INT,
    IN nameChar VARCHAR(10),
    OUT total INT
)
BEGIN
    SELECT SUM(orderdetails.quantityOrdered) INTO total
    FROM orders
    JOIN customers
    ON customers.customerNumber = orders.customerNumber
    JOIN orderdetails
    ON orders.orderNumber = orderdetails.orderNumber
    WHERE MONTH(orderDate) = orderMonth
    AND YEAR(orderDate) = orderYear
    AND customerName LIKE CONCAT('%', nameChar, '%');
END $$

CREATE PROCEDURE ChangeCredLimit(
    IN customerCountry VARCHAR(50),
    IN percent INT
)
BEGIN 
    UPDATE customers
    SET creditLimit = creditLimit * ((percent * 0.01) + 1)
    WHERE country = customerCountry;
END $$
DELIMITER ;


-- CALL GetOfficeByCountry('Australia');
-- CALL CountOrderByStatus('Shipped', @total);
-- CALL GetCustomerLevel(103, @p_customerLevel);
-- CALL GetCustomerShipping(103, @p_shipping);
-- SELECT @p_customerLevel;
-- SELECT @p_shipping;
-- SELECT @total;
-- SET @counter = 1;
-- CALL set_counter(@counter, 1);

-- SELECT productLine, MSRP from products LIMIT 5;
-- CALL IncreasePrice('Classic Cars', 50);
-- SELECT productLine, MSRP from products LIMIT 5;

CALL CustOrderTotSpesifik(2, 2003, 'r', @total);
SELECT @total;

-- SELECT country, creditLimit FROM customers WHERE country = 'USA';
-- CALL ChangeCredLimit('USA', 50);
-- SELECT country, creditLimit FROM customers WHERE country = 'USA';