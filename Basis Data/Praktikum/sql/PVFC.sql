DROP DATABASE IF EXISTS company;

CREATE DATABASE company;

USE company;

CREATE TABLE Customer (
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR (100) NOT NULL,
	address VARCHAR (100) NOT NULL,
	PRIMARY KEY(id)
);

CREATE TABLE Transaction (
	id INT NOT NULL AUTO_INCREMENT,
	order_date date NOT NULL,
	customer_id INT NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (customer_id) REFERENCES Customer (id)
);

CREATE TABLE Product (
	id INT NOT NULL AUTO_INCREMENT,
	description TEXT NOT NULL,
	finish TEXT,
	quantity INT NOT NULL,
	unit_price INT NOT NULL,
	order_id INT NOT NULL,
	PRIMARY KEY(id),
	FOREIGN KEY(order_id) REFERENCES Transaction(id)
);

ALTER TABLE Transaction AUTO_INCREMENT=1001;

INSERT INTO Customer(id, name, address)
VALUES 
	(1, 'Contemporary Casuals', '1355 S Hines Blvd'),
	(2, 'Value Furniture', '15145 S.W. 17th St.'),
	(3, 'Home Furnishings', '1900 Allard Ave.'),
	(4, 'Eastern Furniture', '1925 Beltline Rd.'),
	(5, 'Impressions', '5585 Westcott Ct.'),
	(6, 'Furniture Gallery', '325 Flatiron Dr'),
	(7, 'Period Furniture', '394 Rainbow Dr'),
	(8, 'California Classics', '816 Peach Rd.'), 
	(9, 'M and H Casual Furniture', '3709 First Street'),
	(10, 'Seminole Interiors', '2400 Rocky Point Dr.'),
	(11, 'American Euro Lifestyles', '2424 Missouri Ave N.'),
	(12, 'Battle Creek Furniture', '345 Capitol Ave. SW'),
	(13, 'Heritage Furnishings', '66789 College Ave.'),
	(14, 'Kaneohe Homes', '112 Kiowai St'),
	(15, 'Mountain Scenes', '4132 Main Str');

INSERT INTO Orders(order_date, customer_id)
VALUES
	('2020-10-10', 1),
	('2020-11-18', 4),
	('2021-01-05', 5),
	('2021-01-30', 3),
	('2021-02-03', 2),
	('2021-03-14', 7),
	('2021-04-12', 9),
	('2021-04-23', 6),
	('2021-05-21', 10),
	('2021-05-30', 8),
	('2021-06-28', 11),
	('2021-07-21', 12),
	('2021-08-17', 14),
	('2021-08-19', 15),
	('2021-08-20', 12);


INSERT INTO Product(description, finish, quantity, unit_price, order_id)
VALUES
	('End Table', 'Cherry', 2, 175, 1001),
	('Coffee Table', 'Natural Ash', 3,  200, 1002),
	('Computer Desk', 'Natural Ash', 5, 375, 1004),
	('Entertainment Center', 'Natural Maple', 4, 650, 1006),
	('Writers Desk', 'Cherry', 10, 325, 1005),
	('8-Drawer Desk', 'White Ash', 5, 750, 1002),
	('Dining Table', 'Natural Ash', 9, 800, 1007),
	('Computer Desk', 'Walnut', 8, 250, 1009),
	('Tea Table', 'Brown Ash', 5, 200, 1008),
	('Computer Desk', 'Black Maple', 10, 350, 1010),
	('Writers Desk', 'Natural Ash', 3, 325, 1012),
	('Electric Desk', 'White Ash', 10, 500, 1014),
	('TV Table', 'Walnut', 5, 250, 1015),
	('Electric Desk', 'Black Maple', 10, 500, 1013);

SELECT * FROM Customer;
SELECT * FROM Transaction;
SELECT * FROM Product;