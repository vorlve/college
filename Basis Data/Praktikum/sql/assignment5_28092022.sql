USE school;

SELECT * FROM student;

SELECT DISTINCT city_address
FROM student;

SELECT MAX(ipk) AS max_ipk
FROM student
WHERE city_address = "Jakarta";

SELECT MIN(ipk) AS min_ipk
FROM student
WHERE departement = "Computer";

SELECT COUNT(*) AS sum_mhs
FROM student
WHERE departement = "Computer";

SELECT AVG(ipk) AS avg_ipk
FROM student
WHERE departement = "Physics";

SELECT count(*) AS count_mhs, departement
FROM student
WHERE departement IS NOT NULL
GROUP BY departement;

SELECT DISTINCT count(*) AS count_mhs, city_address
FROM student
GROUP BY city_address;

SELECT count(*) AS count_mhs, city_address
FROM student
WHERE age < 19
GROUP BY city_address;


