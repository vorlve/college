USE classicmodels;

DROP FUNCTION IF EXISTS bulk_price;
DROP FUNCTION IF EXISTS CustomerLevel;
DROP FUNCTION IF EXISTS ConvertPrice;
DROP TABLE IF EXISTS products_audit;
DROP TABLE IF EXISTS log_harga_produk;
DROP TABLE IF EXISTS produk;
DROP TRIGGER IF EXISTS before_products_update;
DROP TRIGGER IF EXISTS before_products_delete;
DROP TRIGGER IF EXISTS after_products_insert;
DROP TRIGGER IF EXISTS after_orderdetails_insert;


DELIMITER $$


CREATE FUNCTION CustomerLevel(p_creditLim DOUBLE) RETURNS VARCHAR(50)
DETERMINISTIC
BEGIN
    DECLARE lvl VARCHAR(50);
    IF p_creditLim > 50000 THEN
        SET lvl = 'PLATINUM';
    ELSEIF (p_creditLim >= 10000 AND p_creditLim < 50000) THEN
        SET lvl = 'GOLD';
    ELSEIF p_creditLim < 10000 THEN
        SET lvl = 'SILVER';
    END IF;
    RETURN (lvl);
END $$


-- Contoh Trigger 2

CREATE TABLE produk(
    kode_produk VARCHAR(10) PRIMARY KEY,
    nama_produk VARCHAR(50) DEFAULT NULL,
    harga INT
);
CREATE TABLE log_harga_produk(
    log_id INT PRIMARY KEY AUTO_INCREMENT,
    kode_produk VARCHAR(10),
    nama_produk VARCHAR(50) DEFAULT NULL,
    harga_lama INT,
    harga_baru INT,
    change_date DATETIME
);

INSERT INTO produk(kode_produk, nama_produk, harga)
VALUES  
    ('BR001', 'SEMINGGU JAGO CODEIGNITER', 120000),
    ('BR002', 'SEMINGGU JAGO PHP MYSQL', 80000);


CREATE TRIGGER before_produk_update
    BEFORE UPDATE ON produk
    FOR EACH ROW
        BEGIN
            INSERT INTO log_harga_produk
            SET kode_produk = OLD.kode_produk,
            nama_produk = OLD.nama_produk,
            harga_baru = NEW.harga,
            harga_lama = OLD.harga,
            change_date = NOW();
        END $$

UPDATE produk
SET harga = 90000
WHERE kode_produk = 'BR001';

SELECT * FROM log_harga_produk;

/* Buatlah suatu fungsi untuk mengkonversi price dalam database 
classicmodels ke mata uang baru, yang akan dihitung dengan: price * 14560 */

CREATE FUNCTION ConvertPrice(price DOUBLE) RETURNS DOUBLE
DETERMINISTIC
BEGIN
    SET price = price * 14560;
    RETURN (price);
END $$

SELECT buyPrice, ConvertPrice(buyPrice)
FROM products
LIMIT 5;

/*Buatlah trigger untuk mengurangi stok suatu produk secara 
otomatis ketika ada penambahan pada tabel order_details*/

SELECT quantityInStock
FROM products
WHERE productCode = 'S10_1949';

CREATE TRIGGER after_orderdetails_insert
AFTER INSERT ON orderdetails
FOR EACH ROW
    BEGIN
        UPDATE products
        SET quantityInStock = quantityInStock - NEW.quantityOrdered
        WHERE productCode = NEW.productCode;
    END $$

INSERT orderdetails(orderNumber, productCode, quantityOrdered, priceEach, orderLineNumber)
    VALUES (10421, 'S10_1949', 20, '121.64', 4);

SELECT quantityInStock
FROM products
WHERE productCode = 'S10_1949';
        

-- /*Buatlah sebuah function untuk mengetahui harga grosir yang disesuaikan
--  dengan jumlah barang yang akan dibeli. Semisal barang akan dapat diskon 
--  10% apabila beli minimal 1 lusin, dan diskon 15% apabila beli minimal 100. */

CREATE FUNCTION bulk_price(p_Price FLOAT, p_quantityOrdered INT) 
RETURNS FLOAT
DETERMINISTIC
BEGIN
    -- SET p_Price = p_Price * p_quantityOrdered;
    IF p_quantityOrdered >= 12 THEN
        SET p_Price = p_Price * 0.9;
    ELSEIF p_quantityOrdered >= 100 THEN
        SET p_Price = p_Price * 0.85;
    END IF;
    RETURN (p_Price);
END $$

SELECT orders.customerNumber,priceEach, bulk_price(priceEach, quantityOrdered) AS bulkPrice
FROM orderdetails
JOIN orders
ON orders.orderNumber = orderdetails.orderNumber
LIMIT 5;



-- /* Buatlah sebuah trigger untuk merekam history perubahan product serta buatlah 
-- tabel baru untuk menyimpan perubahan tersebut. */

CREATE TABLE products_audit (
    idAudit INT PRIMARY KEY AUTO_INCREMENT,
    productName varchar(70) NOT NULL,
    productLine varchar(50) NOT NULL,
    productScale varchar(10) NOT NULL,
    productVendor varchar(50) NOT NULL,
    productDescription text NOT NULL,
    quantityInStock smallint(6) NOT NULL,
    buyPrice decimal(10,2) NOT NULL,
    MSRP decimal(10,2) NOT NULL,
    changedat DATE DEFAULT NULL,
    ACTION VARCHAR(50) DEFAULT NULL
);

CREATE TRIGGER before_products_update
    BEFORE UPDATE ON products
    FOR EACH ROW
BEGIN 
    INSERT INTO products_audit
    SET ACTION = 'UPDATE',
    productName = OLD.productName,
    productLine = OLD.productLine,
    productScale = OLD.productScale,
    productVendor = OLD.productVendor,
    productDescription= OLD.productDescription,
    quantityInStock = OLD.quantityInStock,
    buyPrice = OLD.buyPrice,
    MSRP = OLD.MSRP,
    changedat = NOW();
END $$

CREATE TRIGGER before_products_delete
BEFORE DELETE ON products
FOR EACH ROW
    BEGIN 
        INSERT INTO products_audit
        SET ACTION = 'DELETE',
        productName = OLD.productName,
        productLine = OLD.productLine,
        productScale = OLD.productScale,
        productVendor = OLD.productVendor,
        productDescription= OLD.productDescription,
        quantityInStock = OLD.quantityInStock,
        buyPrice = OLD.buyPrice,
        MSRP = OLD.MSRP,
        changedat = NOW();
    END $$

CREATE TRIGGER after_products_insert
AFTER INSERT ON products
FOR EACH ROW
    BEGIN 
        INSERT INTO products_audit
        SET ACTION = 'INSERT',
        productName = NEW.productName,
        productLine = NEW.productLine,
        productScale = NEW.productScale,
        productVendor = NEW.productVendor,
        productDescription= NEW.productDescription,
        quantityInStock = NEW.quantityInStock,
        buyPrice = NEW.buyPrice,
        MSRP = NEW.MSRP,
        changedat = NOW();
    END $$

DELIMITER ;
UPDATE products
SET quantityInStock = 7305
WHERE productCode = 'S10_1949';

SELECT * FROM products_audit;