DROP DATABASE IF EXISTS PVFC_database;

CREATE DATABASE PVFC_database;

USE PVFC_database;

CREATE TABLE IF NOT EXISTS tabel_produk (
produk_id INT AUTO_INCREMENT,
deskripsi_produk VARCHAR(255) NOT NULL,
finish VARCHAR(255) NOT NULL,
unit_price INT NOT NULL,
PRIMARY KEY (produk_id)
) ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS kust_tabel (
cust_id INT AUTO_INCREMENT,
nama_cust VARCHAR(255) NOT NULL,
alamat_cust TEXT,
PRIMARY KEY (cust_id)
)ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS order_tabel (
order_id INT,
tanggal_order TEXT,
quantity INT,
cust_id INT,
produk_id INT,
FOREIGN KEY (cust_id) REFERENCES kust_tabel(cust_id),
FOREIGN KEY (produk_id) REFERENCES tabel_produk(produk_id)
)ENGINE = INNODB;

describe tabel_produk;
describe kust_tabel;
describe order_tabel;

INSERT INTO kust_tabel(cust_id, nama_cust, alamat_cust)
VALUES (2, 'Value Furniture','15145 S.W. 17th St. Plano TX 750');

INSERT INTO kust_tabel(cust_id, nama_cust, alamat_cust)
VALUES (1, 'Budi Putra','Jalan sukamaju no 5 Kecamatan UGM');

INSERT INTO kust_tabel(cust_id, nama_cust, alamat_cust)
VALUES (3, 'Sutono','Jalan sukamundur no 7 Kecamatan MIPA');

INSERT INTO kust_tabel(cust_id, nama_cust, alamat_cust)
VALUES (4, 'Sukirman','Jalan sukadurian no 10 Kecamatan ILKOMP');

INSERT INTO kust_tabel(cust_id, nama_cust, alamat_cust)
VALUES (5, 'Sutinem','Jalan sukakamu no 99 Kecamatan DIMANA AJA');

INSERT INTO tabel_produk(produk_id, deskripsi_produk, finish, unit_price)
VALUES (7, 'Dining table','Natural Ash', 800),
(4, 'Entertainment Center','Natural maple', 650),
(5, 'Writer desk','Cherry', 325),
(2, 'Botol Tupperware','Apple', 120),
(1, 'Kotak Makan UNO','Durian', 150);

INSERT INTO order_tabel(order_id, tanggal_order, quantity, cust_id, produk_id)
VALUES (1006, '2006-10-24',2, 2, 7),
(1006, '2006-10-24',2, 2, 5),
(1006, '2006-10-24',1, 2, 4),
(1005, '2006-11-24',4, 1, 1),
(1004, '2006-12-24',1, 3, 4),
(1003, '2006-14-24',5, 4, 5),
(1002, '2006-17-24',8, 5, 2);

SELECT * FROM kust_tabel;

insert into kust_tabel
values
    (6,'Kismor','53427 Sudirman Street'),
    (7,'Poretus','72543 MIPA Street'),
    (8,'Kolang','55321 Kaliurang Street'),
    (9,'Pidul','45322 Ringroad Barat'),
    (10,'Bagas','55532 Ringroad Utara'),
    (11,'Muhid Furniture','54523 Palagan'),
    (12,'Ahmad','23413 Godean'),
    (13,'Tono','12345 Ahmad Yani Street'),
    (14,'Kanto','11352 Tugu Jogja'),
    (15,'Bindat','35463 Tangerang');
    
SELECT * FROM tabel_produk;
insert into tabel_produk
values
    (3,'Work Chair','Black',325.00),
    (6,'Cool Chair','RGB',650.00),
    (8,'Blue Chair','Leather',890.00),
    (9,'Big Lamp','White',700.00),
    (10,'Small Lamp','Blue',750.00),
    (11,'Big Door','Red',1000.00),
    (12,'Small Door','Yellow',900.00),
    (13,'Cup','Magenta',825.00),
    (14,'Glass','Grey',600.00),
    (15,'Bottle','Red',200.00);

SELECT * FROM order_tabel;
insert into order_tabel
values
	(1007,'2010-02-20',8,7,1),
    (1008,'2012-01-21',9,8,1),
    (1009,'2009-01-01',10,9,1),
    (1010,'2014-12-30',11,10,1),
    (1011,current_date(),12,11,1),
    (1012,'2018-11-18',13,12,1),
    (1013,current_date(),14,13,1),
    (1014,current_date(),15,14,1);


SELECT order_id, nama_cust, SUM(unit_price*quantity) AS total
FROM order_tabel, tabel_produk, kust_tabel
WHERE order_tabel.produk_id = tabel_produk.produk_id AND order_tabel.cust_id = kust_tabel.cust_id
GROUP BY order_tabel.order_id, nama_cust;

SELECT deskripsi_produk, unit_price
FROM tabel_produk
ORDER BY unit_price DESC;

SELECT nama_cust, SUM(quantity) AS total_produk
FROM order_tabel, kust_tabel
WHERE order_tabel.cust_id = kust_tabel.cust_id
GROUP BY nama_cust;

SELECT COUNT(DISTINCT order_id) AS jumlah_order
FROM order_tabel;

SELECT COUNT(DISTINCT produk_id) AS jumlah_produk
FROM tabel_produk;

