/*
SQLyog Community v12.4.3 (64 bit)
MySQL - 10.4.11-MariaDB : Database - homework7
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`homework7` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `homework7`;

/*Table structure for table `tb_location` */

CREATE TABLE `tb_location` (
  `location_code` varchar(6) NOT NULL,
  `location_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`location_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `tb_location` */

insert  into `tb_location`(`location_code`,`location_name`) values 
('INFO01','Laboratorium 1 M Informatika'),
('INFO02','Laboratorium 2 M Informatika'),
('INFO03','Ruang Dosen M Informatika');

/*Table structure for table `tb_product` */

CREATE TABLE `tb_product` (
  `type_code` varchar(6) NOT NULL,
  `location_code` varchar(6) NOT NULL,
  `product_code` varchar(6) NOT NULL,
  `product_name` varbinary(100) DEFAULT NULL,
  `total` int(5) DEFAULT NULL,
  PRIMARY KEY (`type_code`,`location_code`,`product_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `tb_product` */

insert  into `tb_product`(`type_code`,`location_code`,`product_code`,`product_name`,`total`) values 
('A001','INFO02','AP004','Papan Tulis',11),
('E001','INFO01','EK0014','Komputer',2),
('E001','INFO01','EK0015','Printer',3),
('E001','INFO01','EK0016','LCD Projector',1),
('E001','INFO02','EK0014','Komputer',8),
('E001','INFO02','EK0015','Printer',6),
('E001','INFO02','EK0016','LCD Projector',2),
('M001','INFO01','MM011','Meja Komputer',35),
('M001','INFO01','MM012','Kursi Hidrolik',30),
('M001','INFO02','MM011','Meja Komputer',45),
('M001','INFO03','MM012','Kursi Hidrolik',10);

/*Table structure for table `tb_type` */

CREATE TABLE `tb_type` (
  `type_code` varchar(6) NOT NULL,
  `type_name` varchar(100) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`type_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `tb_type` */

insert  into `tb_type`(`type_code`,`type_name`,`description`) values 
('A001','ATK','alat tulis kantor'),
('E001','Alat-alat elektronik','alat elektronik'),
('M001','Mebeler','alat mebel');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

SHOW TABLES;
SELECT * FROM tb_location;
SELECT * FROM tb_product;
SELECT * FROM tb_type;

-- 1.	Menampilkan kode barang, nama barang beserta jumlahnya yang berada di lokasi "Laboratorium 2 M Informatika"
 SELECT p.product_code, p.product_name, p.total
 FROM tb_product p
 WHERE p.location_code IN
	(SELECT location_code
    FROM tb_location l
    WHERE l.location_name = "Laboratorium 2 M Informatika");

-- 2.	Menampilkan kode barang, nama barang yang berjenis "Alat-alat elektronik" dan seluruh barang yang ada di "Ruang Dosen M Informatika"
SELECT p.product_code, p.product_name
FROM tb_product p
WHERE p.type_code IN
	(SELECT t.type_code
    FROM tb_type t
    WHERE t.type_name = "Alat-alat elektronik")
    AND
    p.location_code IN
    (SELECT l.location_code
    FROM tb_location l
    WHERE l.location_name = "Ruang Dosen M Informatika");

-- 3.	Menampilkan kode barang, nama barang dan jumlah barang yang merupakan jenis "Alat-alat elektronik" dan "Mebeler" yang terletak di "Laboratorium 2 M Informatika"
SELECT p.product_code, p.product_name, p.total
FROM tb_product p
WHERE p.type_code IN
	(SELECT t.type_code
    FROM tb_type t
	WHERE t.type_name = "Alat-alat elektronik" OR t.type_name = "Mebeler")
    AND
    p.location_code IN
    (SELECT l.location_code
    FROM tb_location l
    WHERE l.location_name = "Laboratorium 2 M Informatika");
    
-- 4.	Menampilkan semua informasi barang selain jenis "Mebeler"yang berada di "Laboratorium 2 M Informatika" 
SELECT *
FROM tb_product p
WHERE p.type_code NOT IN
	(SELECT t.type_code
    FROM tb_type t
	WHERE  t.type_name = "Mebeler")
    AND
    p.location_code IN
    (SELECT l.location_code
    FROM tb_location l
    WHERE l.location_name = "Laboratorium 2 M Informatika");
    
-- 5.	Menambahkan barang dengan nama "Spidol" dengan kode barang "AP005" dan informasi barang lain sama dengan "Papan Tulis" tetapi jumlah barang 2 kali lipat dari jumlah "Papan Tulis"
INSERT INTO tb_product(type_code, location_code, product_code, product_name, total)
VALUES
	((SELECT type_code
    FROM tb_product p
    WHERE p.product_name = "Papan Tulis"),
    (SELECT location_code
    FROM tb_product p
    WHERE p.product_name = "Papan Tulis"),
    "AP005",
    "Spidol",
    (SELECT p.total*2
    FROM tb_product p
    WHERE p.product_name = "Papan Tulis"));

SELECT * FROM tb_product;

-- 6.	Menambahkan "Kabel HDMI" yang akan di tempatkan di "Ruang Dosen M Informatika" dengan kode barang "EK0017", jenis 'E001', dan berjumlah sama dengan seluruh "LCD Projector"
INSERT INTO tb_product(type_code, location_code, product_code, product_name, total)
VALUES
	("E001", 
    (SELECT location_code
    FROM tb_location
    WHERE location_name = "Ruang Dosen M Informatika"),
    "EK0017",
    "Kabel HDMI",
    (SELECT SUM(p.total)
    FROM tb_product p
    WHERE p.product_name = "LCD Projector"));
    
SELECT * FROM tb_product;

-- 7.	Mengubah jumlah "Komputer" yang ada di "Laboratorium 1 M Informatika" sesuai dengan jumlah "Meja Komputer" di ruangan tersebut
UPDATE tb_product 
SET total = 
	(SELECT * FROM(SELECT p.total
    FROM tb_product p
    WHERE p.product_name = "Meja Komputer" AND p.location_code IN
		(SELECT location_code
        FROM tb_location
        WHERE location_name = "Laboratorium 1 M Informatika")) AS temp)
WHERE product_name = "Komputer" AND location_code IN
	(SELECT location_code
    FROM tb_location
    WHERE location_name = "Laboratorium 1 M Informatika");

SELECT * FROM tb_product;

-- 8.	Mengubah seluruh jumlah "Kursi Hidrolik" di setiap ruangan dengan jumlah minimum dari "Meja Komputer"
UPDATE tb_product
SET total = 
	(SELECT * FROM(SELECT MIN(p.total)
    FROM tb_product p
    WHERE p.product_name = "Meja Komputer") AS temp)
WHERE product_name = "Kursi Hidrolik";

SELECT * FROM tb_product;

-- 9.	Menghapus seluruh data barang yang ada di lokasi "Ruang Dosen M Informatika"
DELETE FROM tb_product
WHERE location_code =
	(SELECT l.location_code
    FROM tb_location l
    WHERE location_name = "Ruang Dosen M Informatika");
    
SELECT * FROM tb_product;

-- 10.	Menghapus seluruh data barang di lokasi "Laboratorium 2 M Informatika" yang bukan merupakan jenis "Mebeler"
DELETE FROM tb_product
WHERE location_code =
	(SELECT l.location_code
    FROM tb_location l
    WHERE location_name = "Laboratorium 2 M Informatika")
    AND type_code NOT IN
		(SELECT type_code
        FROM tb_type
        WHERE type_name = "Mebeler");
        
SELECT * FROM tb_product;