# Relational Model
**![](https://lh5.googleusercontent.com/PvHAc6bPypcSmL7tsz9I4OS1A_-1XI6sQsurMCKawp_OSjvOhePyrlODjA0fUU52GteBI1gn7wSnzstAtFP_VXTDznSuPpcwZdWyfZecu2kRf1qOc1FoQOXOq663hIQl6H_AbGHK-Vp67JlnM2a058T0Ew-7tHuU9RQBBlgWKHfUcjaasJ5lkaG8xQ)**

### Tentukan Primary keys and foreign keys di setiap tabel dalam database. Untuk setiap foreign key harus ditunjukan tabel dan kolom yang menjadi referensinya.

1. Patient
   1. Primary Key	: Code
   2. Foreign Key	: -
2. Ward
   1. Primary Key	: Code
   2. Foreign Key	: Consultant References from Doctor Number
3. Admission
   1. Primary Key	: -
   2. Foreign Key	: 
      1. Patient References from Patient Code 
      2. Ward References from Ward Code
4. Doctor
   1. Primary Key	: Number
   2. Foreign Key	: Ward References from Ward Code

### Dapatkah data (Number = 502, Surname = White, FirstName = John, Ward = D) ditambahkan ke relasi DOCTOR? Berikan penjelasan pada jawaban Anda.

**Jawab**: Data tersebut tidak dapat ditambahkan ke relasi DOCTOR. Karena meskipun seluruh data yang diperlukan oleh atribut tabel DOCTOR sudah terpenuhi, Ward yang merupakan foreign key tidak terdapat Ward = D. 

### Dapatkah baris (record) ke 3 dari relasi WARD dihapus? Berikan penjelasan pada jawaban Anda.
**Jawab** : Baris ketiga pada relasi WARD tidak dapat dihapus karena berkaitan terhadap data lainnya. 

### ​​Apakah (Surname, FirstName) di relasi PATIENT merupakan superkey? Berikan penjelasan pada jawaban Anda.

**Jawab**: Bisa karena nama dapat menjadi atribut yang unik untuk mengidentifikasi baris atau data dalam tabel.

# Relational Algebra 

Jawablah kueri di bawah ini menggunakan Relational 

Algebra pada basis data universitas
yang tersedia di RelaX. Tuliskan jawaban pada MS-Word dan lampiri dengan screenshot dari luaran yang ada di RelaX. Jika ada kueri yang tidak dapat dituliskan dalam Relational Algebra, berikan alasannya.

1. Menampilkan nama departemen pada building 'Taylor'.

    `σ building='Taylor' department`{.sql}

2. Menampilkan nama mata kuliah yang memiliki prasyarat.

    `course ⨝ prereq`
￼
3. Menampilkan instructor.ID, instructor.name dan teaches.course_id dari tabel instructor dan teaches di mana on instructor.ID = teaches.ID

𝚷 instructor.ID, instructor.name, teaches.course_id (instructor ⨝ teaches)

4. Menampilkan student.ID dan course.ID dari tabel student dan course di mana takes.year = 2009 dan takes.grade = 'A'

π student.ID, takes.course_id (student ⨝ (takes.year = 2009 ∧ takes.grade = 'A') takes)

5. Menampilkan jumlah mahasiswa (membuat kolom baru bernama 'jumlah') yang nilainya A atau A- dari tabel takes pada tahun 2009

	γ count(takes.ID) → jumlah(σ (takes.grade = 'A' or takes.grade = 'A-') (takes))

6. Menampilkan nama mahasiswa dengan inisial B.

σ (name like 'B%') student

7. Menampilkan mahasiswa yang total SKS < 60

σ (tot_cred < 60) student

8. Menampilkan kapasitas dari building ‘Taylor’.

π classroom.capacity (σ(building = 'Taylor') classroom)
